<?php
/**
* LayerSlider v6.1.9 - Responsive Slideshow Module http://layerslider.webshopworks.com
*
*  @author    WebshopWorks <info@webshopworks.com>
*  @copyright 2017 WebshopWorks
*  @license   One Domain Licence
*/

defined('_PS_VERSION_') or exit;
defined('LS_PHPQUERY') or define('LS_PHPQUERY', true);

class LsQuery
{
    private $dom;
    private $nodes;

    public function __construct($html = null)
    {
        if ($html) {
            $this->dom = new DOMDocument();
            $this->dom->encoding = 'utf-8';
            $this->dom->loadHTML(self::utf8Decode($html));
            $this->nodes = $this->dom->documentElement->lastChild->childNodes;
        }
    }

    public function __get($prop)
    {
        if ('length' == $prop) {
            return $this->nodes ? $this->nodes->length : 0;
        }
    }

    public function children()
    {
        if ($this->length && $this->nodes->item(0)->childNodes->length) {
            $domElements = true;
            $children = &$this->nodes->item(0)->childNodes;
            $i = $children->length;
            while ($i--) {
                if (get_class($children->item($i)) != 'DOMElement') {
                    $domElements = false;
                    break;
                }
            }
            if ($domElements) {
                $doc = new self();
                $doc->dom = &$this->dom;
                $doc->nodes = &$children;
                return $doc;
            }
        }
        return $this;
    }

    public function addClass($class)
    {
        $i = $this->length;
        while ($i--) {
            $node = $this->nodes->item($i);
            $classes = $node->getAttribute('class');
            $node->setAttribute('class', $classes ? "$classes $class" : $class);
        }
        return $this;
    }

    public function attr($attr, $value = null)
    {
        // getter
        if (is_string($attr) && $value === null) {
            return $this->length ? $this->nodes->item(0)->getAttribute($attr) : '';
        }
        // setter
        if (is_string($attr)) {
            $attr = array($attr => $value);
        }
        $i = $this->length;
        while ($i--) {
            $item = $this->nodes->item($i);
            foreach ($attr as $key => &$val) {
                if ($key == 'options' && is_array($val)) {
                    foreach ($val as $optval => $opt) {
                        $option = $this->dom->createElement('option', $opt);
                        $option->setAttribute('value', $optval);
                        $item->appendChild($option);
                    }
                } elseif (get_class($item) == 'DOMElement') {
                    $item->setAttribute($key, $val);
                }
            }
        }
        return $this;
    }

    public function removeAttr($attr)
    {
        $i = $this->length;
        while ($i--) {
            $this->nodes->item($i)->removeAttribute($attr);
        }
        return $this;
    }

    public function val($value = null)
    {
        return $this->attr('value', $value);
    }

    public function html($html)
    {
        $i = $this->length;
        while ($i--) {
            $this->nodes->item($i)->nodeValue = '';
        }
        return $this->append($html);
    }

    public function append($html)
    {
        $doc = new DOMDocument();
        $doc->encoding = 'utf-8';
        $doc->loadHTML(self::utf8Decode('<div>'.$html.'</div>'));
        $i = $this->length;
        while ($i--) {
            $item = $this->nodes->item($i);
            foreach ($doc->documentElement->lastChild->firstChild->childNodes as $node) {
                $item->appendChild($this->dom->importNode($node, true));
            }
        }
        return $this;
    }

    public function find($rule)
    {
        $rule = '//' . preg_replace('/\s*,\s*/', ' | //', trim($rule));
        $rule = preg_replace('/\[(\w+)\*="([^"]*)"\]/', '[contains(@$1, "$2")]', $rule);

        $xpath = new DOMXPath($this->dom);
        $nodes = $this->nodes ? $xpath->query($rule, $this->nodes->item(0)) : null;

        $doc = new self();
        $doc->dom = &$this->dom;
        $doc->nodes = &$nodes;
        return $doc;
    }

    public function __toString()
    {
        preg_match('/<body>(.*)<\/body>/s', $this->dom->saveHTML(), $html);
        return isset($html[1]) ? $html[1] : '';
    }

    public static function utf8Decode($str)
    {
        if (function_exists('mb_convert_encoding')) {
            return mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8');
        } else {
            return htmlspecialchars_decode(utf8_decode(htmlentities($str, ENT_COMPAT, 'utf-8', false)));
        }
    }

    public static function newDocument($html)
    {
        return new self($html);
    }

    public static function newDocumentHTML($html)
    {
        return new self($html);
    }

    public static function unloadDocuments()
    {
        // TODO
    }
}
