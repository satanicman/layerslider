<?php
/**
* LayerSlider v6.1.9 - Responsive Slideshow Module http://layerslider.webshopworks.com
*
*  @author    WebshopWorks <info@webshopworks.com>
*  @copyright 2017 WebshopWorks
*  @license   One Domain Licence
*/

defined('_PS_VERSION_') or exit;

if (strpos(LS_PLUGIN_VERSION, 'b') !== false) : ?>
    <div class="ls-version-number">
        <?php ls_e('Using beta version', 'LayerSlider') ?> (<?php echo LS_PLUGIN_VERSION ?>)
        <a href="mailto:info@webshopworks.com?subject=LayerSlider PS (v<?php echo LS_PLUGIN_VERSION ?>) Feedback"><?php ls_e('Send feedback', 'LayerSlider') ?></a>
    </div>
    <?php
endif;
