<?php
/**
* LayerSlider v6.1.9 - Responsive Slideshow Module http://layerslider.webshopworks.com
*
*  @author    WebshopWorks <info@webshopworks.com>
*  @copyright 2017 WebshopWorks
*  @license   One Domain Licence
*/

defined('_PS_VERSION_') or exit;

$lsPluginDefaults = array(

    // PATHS
    'paths' => array(
        'rootUrl' => ls_plugins_url('', __FILE__),
        'skins' => ls_plugins_url('', __FILE__).'/skins/',
        'transitions' => ls_plugins_url('', __FILE__).'/demos/transitions.json'
    ),

    'features' => array(
        'autoupdate' => true
    ),

    // INTERFACE
    'interface' => array(

        'settings' => array(

        ),

        'fonts' => array(

        ),

        'news' => array(
            'display' => true,
            'collapsed' => false
        ),

    ),

    // Settings
    'settings' => array(
        'scriptsInFooter' => false,
        'conditionalScripts' => false,
        'concatenateOutput' => true,
        'cacheOutput' => false
    )
);
