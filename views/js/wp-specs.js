/*! LayerSlider v6.1.9
* layerslider.webshopworks.com
* Copyright 2017 WebshopWorks */

['html', 'text'].forEach(function(plg) {
	// unwrap CDATA in case of <script type="text/html">
  jQuery.fn[plg] = (function(parent) {
  	return function() {
  		var res = parent.apply(this, arguments);
  		if (!arguments.length && this.is('script[type="text/html"]')) {
  			res = res.replace(/^\/\*<!\[CDATA\[\*\//i, '', res);
  			res = res.replace(/\/\*\]\]>\*\/$/, '', res);
  		}
  		return res;
  	};
  })(jQuery.fn[plg]);
});

jQuery(function($) {

	var $document = $(document),
			$window = $(window),
			$body = $(document.body);

	var root = location.pathname.split('/');
	root.length -= 2;
	root = root.join('/');
	var token = (location.search.match(/[?&]token=([^&]+)/) || [,''])[1];

	// put admin inside a div#wpwrap
	var $wpwrap = $('<div id="wpwrap">').appendTo($body);
	$wpwrap.siblings().appendTo($wpwrap);

	// remove message param from URL
	var message = location.href.match(/&message=[^&]*/);
	if (message && history.pushState) {
		history.pushState('', '', location.pathname + location.search.replace(message[0], ''));
	}

	// Update sliders per page fix
	$('#ls-screen-options-form').submit(function(e) {
		e.stopPropagation();
		var options = {};
		$(this).find('input').each(function() {
			if( $(this).is(':checkbox')) {
				options[$(this).attr('name')] = $(this).prop('checked');
			} else {
				options[$(this).attr('name')] = $(this).val();
			}
		});

		// Save settings
		$.post(ajaxurl, $.param({ action : 'ls_save_screen_options', options : options }), function() {
			if(typeof reload != "undefined" && reload === true) {
				location.href = location.href;
			}
		});
	});

	// filtering sliders fix
	$('<input>', {name: 'token', value: token, type: 'hidden'}).prependTo('#ls-slider-filters');
	$('<input>', {name: 'controller', value: 'AdminLayerSlider', type: 'hidden'}).prependTo('#ls-slider-filters');

	$('body').on('click', '#btn-connect-ps', function() {
		kmUI.modal.close();
		kmUI.overlay.close();
		setTimeout(function() {
			$('#modal_addons_connect').modal('show');
		}, 500);
	});

	// post settings
	var $basic = $('.ls-post-basic').change(function(e) {
		$adv.find('select[name=post_type]').val(0);
		$adv.find('select[name=post_categories]').val(0);
		$adv.find('select[name=post_tags]').val(0);
		$adv.find('select[name=post_offset]').val(-1);
		$adv.find('select[name=post_order]').val('DESC');
		LS_PostOptions.change( $adv.find('select[name=post_orderby]').val(e.target.value) );
	});
	var $adv = $('.ls-post-advanced').change(function() {
		$basic.find('input[name=post_basic]').attr('checked', false);
	})
	$('#ls-post-settings-adv').attr('checked', !!localStorage.lsPostSettingsAdv).customCheckbox().change(function() {
		if (this.checked) {
			localStorage.lsPostSettingsAdv = 1;
			$basic.slideUp();
			$adv.slideDown();
		} else {
			localStorage.lsPostSettingsAdv = '';
			$basic.slideDown();
			$adv.slideUp();
		}
	}).change();

	// Import sample sliders
	var key = '', errorMsg = '';
	var $ts = $('#ls-import-samples-button').click(function(e) {
		e.stopImmediatePropagation();
		e.preventDefault();

		if ($('#password_addons').length) {
			return kmUI.modal.open('#tmpl-template-store', { width: 500, height: 250 });
		}

		var popup = $('#ls-import-sliders-template');
		if (!popup.length) popup = $('<div id="ls-import-sliders-template">').appendTo($body);
		var top = $('#content').offset().top;
		popup.attr('style', 'position: fixed !important;').css({
			top : top,
			opacity: 0,
			width: '100%',
			height: 'calc(100vh - '+top+'px)',
			zIndex: 9999991,
			overflowY: 'scroll'
		}).show().animate({ opacity: 1 }, 300);

		popup.on('click', '.ls-import-close', function() {
			popup.hide();
			$body.css('overflow', '').off('keyup.lsimport');
		});

		$body.css('overflow', 'hidden').on('keyup.lsimport', function(e) {
			if (e.keyCode == 27) $('.ls-import-close').click();
		});

		if (!popup.children().length) {
			var domain = location.protocol+'//layerslider.webshopworks.com';
			$.ajax({
				url: domain+'/explore-sliders',
				dataType: 'jsonp',
				success: function(data) {
					data.css.forEach(function(src) {
						$('<link type="text/css" rel="stylesheet">')
							.appendTo(document.head)
							.attr('href', src);
					});
					popup.html(data.html);
					popup.find('.filter-options').css('visibility', 'hidden'); // hide filters while loading
					popup.find('.btn__import').css('display', ''); // show import buttons
					popup.find('.href-overlay, .picture-item__like').remove(); // remove likes
					popup.find('a[target]').each(function() { // update slide links
						$(this).attr({
							target: '_blank',
							href: domain + this.attributes.href.value
						});
					});
					popup.children().css('min-height', '100%'); // css fix
					$x = $('<i class="flaticon-delete ls-import-close">').css({color: '#2f3d46', top: 10}).appendTo(popup);
					popup.scroll(function() { $x.css('top', popup.scrollTop() + 10) });
					$.ajax(data.js[0], {
						dataType: 'script',
						cache: true,
						complete: function() {
							popup.find('.filter-options').css('visibility', ''); // reveal tags
							// $.getScript(location.protocol+'//offlajn.com/index2.php?option=com_ls_import&task=request&slider=test'); // check domain
							$.post(ajaxurl, {action: 'ls_test_template_store'}, function(data) {
								data = JSON.parse(data);
								data.success ? key = data.key : lsImportError(errorMsg = data.msg);
							});
						}
					});
				},
				error: function() {
					console.log(arguments)
				}
			});
			popup.on('click', 'a[data-import]', function onImport() {
				if (!key) {
					return lsImportError(errorMsg);
				}
				var $figure = $(this).closest('figure').addClass('loading');
				$('.logoload').css({
					position: 'absolute',
					display: 'block',
					opacity: 1
				}).appendTo($figure);
				$.getScript(location.protocol+'//offlajn.com/index2.php?option=com_ls_import&task=psrequest&ver='+lsVersion+'&slider='+$(this).data('import')+'&key='+key);
			});
			var slideDur = 400;
			popup.on('click', '.ls-warning-close', function closeWarning() {
				$(this).closest('.ls-warning-cont').slideUp(slideDur, function() { $(this).remove() });
			});
			function scrollUp() {
				var scroll = { y: popup.scrollTop() };
				scroll.y && TweenLite.to(scroll, 0.3, { y: 0, onUpdate: function() { popup.scrollTop(scroll.y) } });
			}
			window.lsImportError = function(msg) {
				scrollUp();
				$('.ls-warning-close').click();
				$('figure.loading').removeClass('loading').find('.logoload').hide();
				$('<div class="ls-warning-cont"><h3 class="flaticon-warning">' +
					'<p>'+msg+'</p><i class="flaticon-delete ls-warning-close"/>')
					.insertBefore('#grid').slideUp(0).slideDown(slideDur);
			};
		}
	});

	// init addons connect
	if (~location.search.indexOf('&conf=32')) {
		history.pushState && history.pushState('', '', location.pathname + location.search.replace('&conf=32', ''));
		$ts.click();
	}
	// init edit
	if (~location.search.indexOf('&edited=1')) {
		history.pushState && history.pushState('', '', location.pathname + location.search.replace('&edited=1', ''));
	}

	function createParamSelect(data, value, active) {
		$select = $('<select>', {name: data.name});
		if (value) {
			data.opts.forEach(function(opt) {
				if (opt.active == active) $('<option>', { value: opt[value], html: opt.option || opt.name }).appendTo($select);
			});
		} else {
			for (var key in data.opts) {
				$('<option>', { value: key, html: data.opts[key] }).appendTo($select);
			}
		}
		return $select;
	}

	function createParamTr(data, $param) {
		$tr = $('<tr>');
		$('<td>', {html: data.title}).appendTo($tr);
		$param.wrap('<td>').parent().appendTo($tr);
		$('<td>', {html: data.desc, 'class': 'desc'}).appendTo($tr);
		return $tr;
	}

	var $hook;
	if (location.search.match(/\bcontroller=AdminLayerSlider\b/i)) {
		$.get(ajaxurl, {action: 'ls_get_hooks'}, function(data) {
			lsDataHook = JSON.parse(data);
			$hook = createParamSelect(lsDataHook);
			if (window.lsSliderData) {
				$hook.val(lsSliderData.properties.hook || '');
				createParamTr(lsDataHook, $hook).prependTo('.ls-settings-contents tbody:eq(1)');
				$('<a>', { href: 'javascript:;', html: ' <i class="dashicons dashicons-admin-generic ls-conf"></i>'+lsDataHook.conf }).click(function() {
					$('li[data-deeplink=misc]').click();
				}).insertAfter($hook);
			} else {
				var $list = $('.ls-sliders-list');
				if ($list.length) {
					var hooks = {};
					lsDataHook.sliders.forEach(function(slider) {
						hooks[slider.id] = slider.hook;
					});
					lsDataHook.name = 'hooks[]';
					var $select = createParamSelect(lsDataHook).addClass('ls-hook');
					$list.find('thead td.center').before('<td>'+lsDataHook.title+'</td>');
					$list.find('td.name').each(function() {
						var $td = $('<td>').insertAfter(this);
						var id = $(this.parentNode).data('id');
						$select.clone().appendTo($td).val(hooks[id]).data('value', hooks[id]);
						$('<i class="dashicons dashicons-update ls-hook-update">').appendTo($td);
					});
				}
			}
		});
	}

	$(document.body).on('change', 'select.ls-hook', function onChangeHook() {
		var $this = $(this);
		$this.next().css('display', $this.val() == $this.data('value') ? 'none' : 'inline-block');
	}).on('click', '.ls-hook-update', function onClickHookUpdate() {
		var $this = $(this);
		var rotate = TweenMax.to(this, 1, {
			rotation: 360,
			repeat: -1,
			ease: Linear.easeNone
		});
		var id = $this.closest('[data-id]').data('id');
		$.post(ajaxurl, {
			action: 'ls_update_hook',
			id: id,
			hook: $this.prev().val()
		}, function onUpdateHook(data) {
			rotate.kill();
			TweenLite.set($this[0], { rotation: 0 });
			data = JSON.parse(data);
			if (data.success) {
				$('[data-id='+ id +'] .ls-hook').val(data.hook);
				for (var i = lsDataHook.sliders.length; i--;) {
					if (lsDataHook.sliders[i].id == id) {
						lsDataHook.sliders[i].hook = data.hook;
						break;
					}
				}
				$this.css('display', 'none');
				var $ok = $('<i class="dashicons dashicons-yes">').css('display', 'inline-block').insertAfter($this);
				TweenLite.to($ok[0], 0.3, {
					delay: 1,
					opacity: 0,
					onComplete: function() { $(this.target).remove() }
				});
			} else {
				alert(data.errorMsg);
			}
		});
	});

	$('.ls-slider-list-form').on('click', '.embed', function() {
		var id = $(this).data('id');
		setTimeout(function() {
			var hook = '';
			for (var i = lsDataHook.sliders.length; i--;) {
				if (lsDataHook.sliders[i].id == id) {
					hook = lsDataHook.sliders[i].hook;
					break;
				}
			}
			var $modpos = $('.ls-modpos').attr('data-id', id).data('id', id);
			$hook.clone().addClass('ls-hook').val(hook).data('value', hook).appendTo($modpos);
			$('<i class="dashicons dashicons-update ls-hook-update">').appendTo($modpos);
		}, 1);
	});

	var imgs = '/img/';

	if (window.lsSliderData) { // EDIT VIEW

		$.get(ajaxurl, {action: 'ls_get_shop_params'}, function(params) {
			params = JSON.parse(params);
			lsSliderData.properties.position = 'position' in lsSliderData.properties ? lsSliderData.properties.position : 10;
			$position = $('<input>', { name: 'position', type: 'number', value: lsSliderData.properties.position });
			createParamTr(params.position, $position).prependTo('.ls-settings-contents tbody:last');

			$tr = $('<tr>');
			$('<td>', {html: params.cats.title}).appendTo($tr);
			$td = $('<td colspan="2">').appendTo($tr);
			lsSliderData.properties.cats = typeof lsSliderData.properties.cats == 'string' ? JSON.parse(lsSliderData.properties.cats) : lsSliderData.properties.cats || ['all'];
			createParamSelect(params.cats, 'value').attr({ multiple: true, style: 'width:auto; height:250px;' }).val(lsSliderData.properties.cats).appendTo($td);
			lsSliderData.properties.pages = typeof lsSliderData.properties.pages == 'string' ? JSON.parse(lsSliderData.properties.pages) : lsSliderData.properties.pages || ['all'];
			createParamSelect(params.pages, 'value').attr({ multiple: true, style: 'width:auto; height:250px;' }).val(lsSliderData.properties.pages).appendTo($td);
			$('<br/><span style="font-size:11px; color:#898989; font-weight:400">'+params.cats.desc+'</span>').appendTo($td);
			$tr.prependTo('.ls-settings-contents tbody:last');

			$tr = $('<tr>');
			$('<td>', {html: params.groups.title}).appendTo($tr);
			$td = $('<td colspan="2">').appendTo($tr);
            lsSliderData.properties.groups = typeof lsSliderData.properties.groups == 'string' ? JSON.parse(lsSliderData.properties.groups) : lsSliderData.properties.groups || ['all'];
            createParamSelect(params.groups, 'id_group').attr({ multiple: true, style: 'width:auto; height:250px;' }).val(lsSliderData.properties.groups).appendTo($td);
			$('<br/><span style="font-size:11px; color:#898989; font-weight:400">'+params.groups.desc+'</span>').appendTo($td);
			$tr.prependTo('.ls-settings-contents tbody:last');


			lsSliderData.properties.lang = lsSliderData.properties.lang || 0;
			$lang = createParamSelect(params.lang, 'id_lang', 1).val(lsSliderData.properties.lang);
			createParamTr(params.lang, $lang).prependTo('.ls-settings-contents tbody:last');
			lsSliderData.properties.shop = lsSliderData.properties.shop || 0;
			$shop = createParamSelect(params.shop, 'id_shop', 1).val(lsSliderData.properties.shop || 0);
			createParamTr(params.shop, $shop).prependTo('.ls-settings-contents tbody:last');
            // lsSliderData.properties.group = lsSliderData.properties.group || 0;
			// $group = createParamSelect(params.group, 'id_group', null).val(lsSliderData.properties.group || 0);
			// createParamTr(params.group, $group).prependTo('.ls-settings-contents tbody:last');
		});

		// v5.x compatibility fix: add root to image URLs, init thumbs, position fix
		var posFix = false, parallax = {};
		if (lsSliderData.properties) {
			var props = lsSliderData.properties;
			if (props.pauseonhover === true) $('select[name=pauseonhover]').val(props.pauseonhover = 'enabled');
			if (props.pauseonhover === false) $('select[name=pauseonhover]').val(props.pauseonhover = 'disabled');
			if (root && props.backgroundimage && props.backgroundimage.indexOf(imgs) == 0) {
				props.backgroundimage = root + props.backgroundimage;
			}
			props.backgroundimageThumb = props.backgroundimage;
			// slider background compatibility fixes
			if (props.background_size) props.globalBGSize = props.background_size, delete props.background_size;
			if (props.background_repeat) props.globalBGRepeat = props.background_repeat, delete props.background_repeat;
			if (props.background_position) props.globalBGPosition = props.background_position, delete props.background_position;
			if (props.background_behaviour) props.globalBGAttachment = props.background_behaviour, delete props.background_behaviour;
		}
		for (var i = 0; i < lsSliderData.layers.length; i++) {
			var slide = lsSliderData.layers[i];
			if (root && slide.properties && slide.properties.background && slide.properties.background.indexOf(imgs) == 0) {
				slide.properties.background = root + slide.properties.background;
			}
			slide.properties.backgroundThumb = slide.properties.background;
			for (var j = 0; j < slide.sublayers.length; j++) {
				var layer = slide.sublayers[j];
				if (root && layer.image && layer.image.indexOf(imgs) == 0) {
					layer.image = root + layer.image;
				}
				if (posFix && layer.styles.top.indexOf('%') > 0 && layer.styles.left.indexOf('%') > 0) {
					layer.transition.position = 'fixed';
				}
				layer.imageThumb = layer.image;
			}
		}

	}

	// Skin/CSS Editor
	if(location.href.indexOf('controller=AdminLayerSliderSkin') != -1) {
		$('select[name="skin"]').change(function(e) {
			e.stopImmediatePropagation();
			location.href = 'index.php?controller=AdminLayerSliderSkin&token='+token+'&skin=' + $(this).children(':selected').val();
		});
	}

	/**
	 * Screen Options tab
	 */
	screenMeta = {
		element: null, // #screen-meta
		toggles: null, // .screen-meta-toggle
		page:    null, // #wpcontent

		init: function() {
			this.element = $('#screen-meta');
			this.toggles = $( '#screen-meta-links' ).find( '.show-settings' );
			this.page    = $('#wpcontent');

			this.toggles.click( this.toggleEvent );
		},

		toggleEvent: function() {
			var panel = $( '#' + $( this ).attr( 'aria-controls' ) );

			if ( !panel.length )
				return;

			if ( panel.is(':visible') )
				screenMeta.close( panel, $(this) );
			else
				screenMeta.open( panel, $(this) );
		},

		open: function( panel, button ) {

			$( '#screen-meta-links' ).find( '.screen-meta-toggle' ).not( button.parent() ).css( 'visibility', 'hidden' );

			panel.parent().show();
			panel.slideDown( 'fast', function() {
				panel.focus();
				button.addClass( 'screen-meta-active' ).attr( 'aria-expanded', true );
			});

			$document.trigger( 'screen:options:open' );
		},

		close: function( panel, button ) {
			panel.slideUp( 'fast', function() {
				button.removeClass( 'screen-meta-active' ).attr( 'aria-expanded', false );
				$('.screen-meta-toggle').css('visibility', '');
				panel.parent().hide();
			});

			$document.trigger( 'screen:options:close' );
		}
	};

	$(function() { screenMeta.init() });

	/**
	 * Help tabs.
	 */
	$('.contextual-help-tabs').delegate('a', 'click', function(e) {
		var link = $(this),
			panel;

		e.preventDefault();

		// Don't do anything if the click is for the tab already showing.
		if ( link.is('.active a') )
			return false;

		// Links
		$('.contextual-help-tabs .active').removeClass('active');
		link.parent('li').addClass('active');

		panel = $( link.attr('href') );

		// Panels
		$('.help-tab-content').not( panel ).removeClass('active').hide();
		panel.addClass('active').show();
	});

});

function lsCloseMediaWindow() {
	var $m = jQuery('#ls-media-window');
	$m.prev().remove();
	$m.remove();
}

/**
 * Fake object to simulate WP's media manager
 */

imgpath= '';

function lsInsertImage(src) {
	wpMediaFrame._state.selection.data = [{
		id: '',
		url: src,
		sizes: {
			full: { url: src }
		}
	}];

	var folder = src.match(/\/images\/(.*\/|)/);
	if (folder) imgpath = folder[1];

	wpMediaFrame.trigger('select');
	lsCloseMediaWindow();
}

wpMediaFrame = {
	open: function() {
		$imgInput = jQuery(this).prev();
		$imgNode = jQuery(this).children().children();
		// Create window
		jQuery('body').prepend( jQuery('<div>', { 'id': 'ls-media-window', 'class': 'ls-modal ls-box' })
			.append( jQuery('<h1>', { 'class': 'header', 'text': 'Image Manager' })
				.append( jQuery('<a>', { 'class': 'dashicons dashicons-no', 'style': 'margin-top:4px;' }).click(lsCloseMediaWindow))
			)
			.append( jQuery('<div>')
				.css('overflow', 'hidden')
				.append( jQuery('<iframe>').attr({
					width: '100%',
					height: '531',
					src: mediamanagerurl
				}).css('border', 'none'))
			)
		);

		// Create overlay
		jQuery('body').prepend( jQuery('<div>', { 'class' : 'ls-overlay'}).click(lsCloseMediaWindow));
	},
	on: function(event, handler) {
		this['on'+event] = handler;
	},
	trigger: function(event) {
		this['on'+event]();
	},
	state: function() {
		return this._state;
	},
	_state: {
		get: function(name) {
			return this[name];
		},
		selection: {
			first: function() {
				this._first = true;
				return this;
			},
			toJSON: function() {
				var first = this._first;
				this._first = false;
				return first ? this.data[0] : this.data;
			},
			data: []
		}
	}
};

wp = {
	media: function() {
		return wpMediaFrame;
	}
};