<?php
/**
* LayerSlider v6.1.9 - Responsive Slideshow Module http://layerslider.webshopworks.com
*
*  @author    WebshopWorks <info@webshopworks.com>
*  @copyright 2017 WebshopWorks
*  @license   One Domain Licence
*/

defined('_PS_VERSION_') or exit;

class LayerSlider extends Module
{
    public static $instance;

    protected $init = false;
    protected $tabs = array(
        'Layer Slider' => array('class' => 'AdminParentLayerSlider', 'active' => 1, 'icon' => 'collections'),
        'Sliders' => array('class' => 'AdminLayerSlider', 'active' => 1),
        'Media Manager' => array('class' => 'AdminLayerSliderMedia', 'active' => 0),
        'Transition Builder' => array('class' => 'AdminLayerSliderTransition', 'active' => 1),
        'Skin Editor' => array('class' => 'AdminLayerSliderSkin', 'active' => 1),
        'CSS Editor' => array('class' => 'AdminLayerSliderStyle', 'active' => 1),
    );
    protected $lang = array(
        'fr' => array(
            'Layer Slider' => 'Layer Slider',
            'Sliders' => 'Diaporamas',
            'Media Manager' => 'Directeur des médias',
            'Transition Builder' => 'Effets de Transition',
            'Skin Editor' => 'Éditeur de skin',
            'CSS Editor' => 'Éditeur de CSS',
        )
    );

    protected $showedUser = false;

    public function __construct()
    {
        $this->name = 'layerslider';
        $this->tab = 'slideshows';
        $this->version = '6.1.9';
        $this->author = 'WebshopWorks';
        $this->module_key = 'b92dd49b8609431aeb010cb8db905a3f';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.7');
        $this->bootstrap = false;
        $this->displayName = $this->l('LayerSlider');
        $this->description = $this->l('Responsive Slideshow Module');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        self::$instance = &$this;
        parent::__construct();
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }
        $db = Db::getInstance();
        $res = $db->execute('
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layerslider` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `author` int(11) NOT NULL DEFAULT 0,
                `name` varchar(100) NOT NULL,
                `slug` varchar(100) NOT NULL,
                `data` mediumtext NOT NULL,
                `date_c` int(11) NOT NULL,
                `date_m` int(11) NOT NULL,
                `flag_hidden` tinyint(1) NOT NULL DEFAULT 0,
                `flag_deleted` tinyint(1) NOT NULL DEFAULT 0,
                PRIMARY KEY (`id`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
            CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layerslider_module` (
                `id_slider` int(11) NOT NULL,
                `id_shop` int(11) NOT NULL DEFAULT 0,
                `id_lang` int(11) NOT NULL DEFAULT 0,
                `hook` varchar(64) NOT NULL DEFAULT \'\',
                `position` tinyint(2) NOT NULL DEFAULT 0,
                `pages` text NULL
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;
        ');
        if (!$res) {
            $this->_errors[] = $db->getMsgError();
            return false;
        }
        $schedule = $db->executeS('SHOW COLUMNS FROM `'._DB_PREFIX_.'layerslider` LIKE \'schedule%\'');
        if (empty($schedule)) {
            $res = $db->execute('ALTER TABLE `'._DB_PREFIX_.'layerslider` ADD `schedule_start` INT(11) NOT NULL, ADD `schedule_end` INT(11) NOT NULL');
            if (!$res) {
                $this->_errors[] = $db->getMsgError();
                return false;
            }
        }
        $this->registerHook('displayBackOfficeHeader');
        return parent::install();
    }

    protected function addTabs()
    {
        $parent = version_compare(_PS_VERSION_, '1.7.0', '<') ? 0 : (int)Tab::getIdFromClassName('CONFIGURE');
        foreach ($this->tabs as $name => $t) {
            $tab = new Tab();
            $tab->active = $t['active'];
            $tab->class_name = $t['class'];
            $tab->name = array();
            foreach (Language::getLanguages(true) as $lang) {
                $tab->name[$lang['id_lang']] = isset($this->lang[$lang['iso_code']]) ? $this->lang[$lang['iso_code']][$name] : $name;
            }
            if (isset($t['icon'])) {
                $tab->icon = $t['icon'];
            }
            $tab->module = $this->name;
            $tab->id_parent = $parent;
            $tab->add();

            if ($t['class'] == 'AdminParentLayerSlider') {
                $parent = (int)Tab::getIdFromClassName($t['class']);
            }
        }
    }

    protected function deleteTabs()
    {
        foreach ($this->tabs as $t) {
            $id_tab = (int)Tab::getIdFromClassName($t['class']);
            if ($id_tab) {
                $tab = new Tab($id_tab);
                $tab->delete();
            }
        }
    }

    public function enable($force_all = false)
    {
        $this->addTabs();
        $this->registerHook('filterCmsContent');
        $this->registerHook('filterProductContent');
        $this->registerHook('filterCategoryContent');
        $this->registerHook('displayHeader');
        $modules = Db::getInstance()->executeS('SELECT DISTINCT hook FROM '._DB_PREFIX_.'layerslider_module WHERE hook NOT LIKE \'\' AND id_shop > -1');
        foreach ($modules as $mod) {
            $this->registerHook($mod['hook']);
        }
        return parent::enable($force_all);
    }

    public function disable($force_all = false)
    {
        $this->deleteTabs();
        $this->unregisterHook('filterCmsContent');
        $this->unregisterHook('filterProductContent');
        $this->unregisterHook('filterCategoryContent');
        $this->unregisterHook('displayHeader');
        $modules = Db::getInstance()->executeS('SELECT DISTINCT hook FROM '._DB_PREFIX_.'layerslider_module WHERE hook NOT LIKE \'\' AND id_shop > -1');
        foreach ($modules as $mod) {
            $this->unregisterHook($mod['hook']);
        }
        return parent::disable($force_all);
    }

    public function getContent()
    {
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminLayerSlider'));
    }

    protected function getPages()
    {
        $obj = array(
            'cat' => array(),
            'prod' => array(),
            'cms' => array(),
            'page' => array()
        );
        $slides = Tools::getValue('layerslider-slides');

        $slides['properties']['cats'] = Tools::stripslashes($slides['properties']['cats']);
        $cats = Tools::jsonDecode($slides['properties']['cats']);
        if (in_array('all', $cats)) {
            $obj['cat'] = $obj['prod'] = 'all';
        } else {
            foreach ($cats as $val) {
                if (preg_match('/^[cp]-\d+$/', $val)) {
                    $v = explode('-', $val);
                    $obj[$v[0] == 'c' ? 'cat' : 'prod'][] = $v[1];
                } else {
                    $obj[$val] = 1;
                }
            }
        }

        $slides['properties']['pages'] = Tools::stripslashes($slides['properties']['pages']);
        $pages = Tools::jsonDecode($slides['properties']['pages']);
        if (in_array('all', $pages)) {
            $obj['cms'] = $obj['page'] = 'all';
        } else {
            foreach ($pages as $val) {
                if (preg_match('/^[cp]-\d+$/', $val)) {
                    $v = explode('-', $val);
                    $obj[$v[0] == 'c' ? 'cms' : 'page'][] = $v[1];
                } else {
                    $obj[$val] = 1;
                }
            }
        }

        return Tools::jsonEncode($obj);
    }

    public function generateSlider($id)
    {
        if (is_array($id)) {
            $id = empty($id[2]) ? $id[1] : $id[2];
        }
        return LsShortcode::handleShortcode(array('id' => $id, 'filters' => ''));
    }

    protected function inGroup(&$mod)
    {
        $pages = Tools::jsonDecode($mod['pages'], true);
        if(isset($pages['group']) && $pages['group']) {
            $result = false;

            if(!$this->showedUser) {
                $groups = $this->context->customer->getGroups();
                foreach ($groups as $id_group) {
                    if (in_array($id_group, $pages['group'])) {
                        $this->showedUser = $result = true;
                        break;
                    }
                }
            }

            return $result;
        }

        return true;
    }

    protected function isOnPage(&$mod)
    {
        if (($mod['id_shop'] == 0 || $mod['id_shop'] == $this->context->shop->id) && ($mod['id_lang'] == 0 || $mod['id_lang'] == $this->context->language->id)) {
            if (!isset($mod['pages']) || !$mod['pages']) {
                $mod['pages'] = '{"cat":"all","prod":"all","cms":"all","page":"all"}';
            }
            if ($mod['pages'] == '{"cat":"all","prod":"all","cms":"all","page":"all"}') {
                return true;
            }
            $pages = Tools::jsonDecode($mod['pages'], true);
            $class = str_replace('controller', '', Tools::strtolower(get_class($this->context->controller)));
            switch ($class) {
                case 'index':
                    if ($pages['cat'] === 'all') {
                        return true;
                    }
                    return isset($pages['index']);
                case 'category':
                    if ($pages['cat'] === 'all') {
                        return true;
                    }
                    $id = Tools::getValue('id_category');
                    return in_array("$id", $pages['cat']);
                case 'product':
                    if ($pages['prod'] === 'all') {
                        return true;
                    }
                    $id = Tools::getValue('id_product');
                    return in_array("$id", $pages['prod']);
                case 'stores':
                    if ($pages['stores']) {
                        return true;
                    }
                    // else continue below
                case 'cms':
                    if ($pages['cms'] === 'all') {
                        return true;
                    }
                    if (isset($this->context->controller->cms->id)) {
                        return in_array("{$this->context->controller->cms->id}", $pages['page']);
                    }
                    if (isset($this->context->controller->cms_category->id)) {
                        return in_array("{$this->context->controller->cms_category->id}", $pages['cms']);
                    }
                    return false;
                case 'psblogpostsmodulefront':
                    return isset($pages[$class]) && !$this->context->controller->id_post;
                case 'prestablogblogmodulefront':
                    if ($pages['cms'] === 'all') {
                        return true;
                    }
                    if ($id = Tools::getValue('id', 0)) {
                        return isset($pages['bn']) && in_array("$id", $pages['bn']);
                    }
                    $c = Tools::getValue('c', 0);
                    return isset($pages['bc']) && in_array("$c", $pages['bc']);
                default:
                    if ($pages['cms'] === 'all') {
                        return true;
                    }
                    return isset($pages[$class]);
            }
        }
        return false;
    }

    protected function displaySliders($hook)
    {
        $tpl = dirname(__FILE__).'/views/templates/hook/layerslider.tpl';
        if (!file_exists($tpl)) {
            file_put_contents($tpl, '{if !empty($content)}<div style="clear:both"></div>{$content nofilter}{/if}');
        }
        $var = array('content' => '');
        $db = Db::getInstance();
        $modules = $db->executeS('SELECT * FROM '._DB_PREFIX_.'layerslider_module WHERE hook LIKE \''.pSQL($hook).'\' ORDER BY position');
        foreach ($modules as &$mod) {
            if ($this->isOnPage($mod) && $this->inGroup($mod)) {
                $var['content'] .= $this->generateSlider($mod['id_slider']);
            }
        }
        $this->context->smarty->assign($var);
        return $this->display(__FILE__, 'layerslider.tpl');
    }

    public function __call($method, $args)
    {
        if (stripos($method, 'hookdisplay') === 0) {
            $hook = 'display'.Tools::substr($method, 11);
            return $this->displaySliders($hook);
        }
    }

    public function hookFilterCmsContent(&$cnt)
    {
        $cnt['object']['content'] = $this->filterShortcode($cnt['object']['content']);
        return $cnt;
    }

    public function hookFilterProductContent(&$cnt)
    {
        $cnt['object']['description'] = $this->filterShortcode($cnt['object']['description']);
        return $cnt;
    }

    public function hookFilterCategoryContent(&$cnt)
    {
        $cnt['object']['description'] = $this->filterShortcode($cnt['object']['description']);
        return $cnt;
    }

    protected function filterShortcode(&$content)
    {
        require_once _PS_MODULE_DIR_.'layerslider/helper.php';
        require_once _PS_MODULE_DIR_.'layerslider/base/layerslider.php';
        $regexLs = '/<p>\s*\[layerslider\s+id=["\']?(\w+)["\']?\s*\]\s*<\/p>|\[layerslider\s+id=["\']?(\w+)["\']?\s*\]/i';
        $regexNav = '/\[ls-navigate\s+id=["\']?(\w+)["\']?\s+action=["\']?(\w+)["\']?\s*\](.*?)\[\/ls-navigate\s*\]/i';
        $lsNav = '<a class="ls-navigate" href="javascript:;" onclick="jQuery(\'#layerslider_$1\').layerSlider(\'$2\')">$3</a>';
        $content = preg_replace_callback($regexLs, array($this, 'generateSlider'), $content);
        $content = preg_replace($regexNav, $lsNav, $content);
        return $content;
    }

    public function hookDisplayHeader()
    {
        require_once _PS_MODULE_DIR_.'layerslider/helper.php';
        require_once _PS_MODULE_DIR_.'layerslider/base/layerslider.php';

        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            foreach ($this->context->controller->js_files as &$js) {
                if (preg_match('/jquery-\d\.\d\.\d(\.min)?\.js$/i', $js)) {
                    $js = __PS_BASE_URI__.'modules/layerslider/views/js/jquery.js';
                    break;
                }
            }
        }
        ls_do_action('ls_enqueue_scripts');

        if (version_compare(_PS_VERSION_, '1.7.1', '<')) {
            // parse shortcodes
            $ctrl = &$this->context->controller;
            if ($ctrl->php_self == 'cms' && isset($ctrl->cms->content)) {
                $ctrl->cms->content = $this->filterShortcode($ctrl->cms->content);
            }
            if (($ctrl->php_self == 'category' || $ctrl->php_self == 'product') && method_exists($ctrl, 'get'.$ctrl->php_self)) {
                $res = $ctrl->{'get'.$ctrl->php_self}();
                $res->description = $this->filterShortcode($res->description);
            }
        }
        return ls_meta_generator();
    }

    public function hookDisplayBackOfficeHeader()
    {
        if (version_compare(_PS_VERSION_, '1.7.1', '<')) {
            return '<style>
                #subtab-AdminParentLayerSlider > a > .material-icons::before,
                .link-levelone > a[href*=AdminLayerSlider] > .material-icons::before { content: "collections"; }
                .icon-AdminParentLayerSlider:before { content: "\f03e"; }
            </style>';
        }
    }

    protected function initTranslate()
    {
        $this->l('Status');
        $this->l('Unpublished sliders will not be visible for your visitors until you re-enable this option. This also applies to scheduled sliders, thus leaving this option enabled is recommended in most cases.');
        $this->l('Schedule From');
        $this->l('No schedule');
        $this->l('Schedule Until');
        $this->l('Slider type');
        $this->l('Canvas width');
        $this->l('The width of the slider canvas in pixels.');
        $this->l('Canvas height');
        $this->l('The height of the slider canvas in pixels.');
        $this->l('Max-width');
        $this->l('The maximum width your slider can take in pixels when responsive mode is enabled.');
        $this->l('Responsive under');
        $this->l('Turns on responsive mode in a full-width slider under the specified value in pixels. Can only be used with full-width mode.');
        $this->l('Mode');
        $this->l('Select the sizing behavior of your full size sliders (e.g. hero scene).');
        $this->l('Normal');
        $this->l('Hero scene');
        $this->l('Fit to parent height');
        $this->l('Allow fullscreen mode');
        $this->l('Visitors can enter OS native full-screen mode when double clicking on the slider.');
        $this->l('Maximum responsive ratio');
        $this->l('The slider will not enlarge your layers above the target ratio. The value 1 will keep your layers in their initial size, without any upscaling.');
        $this->l('Fit to screen width');
        $this->l('If enabled, the slider will always have the same width as the viewport, even if a theme uses a boxed layout, unless you choose the "Fit to parent height" full size mode.');
        $this->l('Prevent slider clipping');
        $this->l('Ensures that the theme cannot clip parts of the slider when used in a boxed layout.');
        $this->l('Move the slider by');
        $this->l('Move your slider to a different part of the page by providing a jQuery DOM manipulation method & selector for the target destination.');
        $this->l('Clip slide transition');
        $this->l('Choose on which axis (if any) you want to clip the overflowing content (i.e. that breaks outside of the slider bounds).');
        $this->l('Do not hide');
        $this->l('Hide on both axis');
        $this->l('X Axis');
        $this->l('Y Axis');
        $this->l('Background size');
        $this->l('This will be used as a default on all slides, unless you choose to explicitly override it on a per slide basis.');
        $this->l('Auto');
        $this->l('Cover');
        $this->l('Contain');
        $this->l('Stretch');
        $this->l('Background position');
        $this->l('This will be used as a default on all slides, unless you choose the explicitly override it on a per slide basis.');
        $this->l('left top');
        $this->l('left center');
        $this->l('left bottom');
        $this->l('center top');
        $this->l('center center');
        $this->l('center bottom');
        $this->l('right top');
        $this->l('right center');
        $this->l('right bottom');
        $this->l('Parallax sensitivity');
        $this->l('Increase or decrease the sensitivity of parallax content when moving your mouse cursor or tilting your mobile device.');
        $this->l('Parallax center layers');
        $this->l('Choose a center point for parallax content where all layers will be aligned perfectly according to their original position.');
        $this->l('At center of the viewport');
        $this->l('At the top of the viewport');
        $this->l('Parallax center degree');
        $this->l('Provide a comfortable holding position (in degrees) for mobile devices, which should be the center point for parallax content where all layers should align perfectly.');
        $this->l('Your parallax layers will move to the opposite direction when scrolling the page.');
        $this->l('Optimize for mobile');
        $this->l('Enable optimizations on mobile devices to avoid performance issues (e.g. fewer tiles in slide transitions, reducing performance-heavy effects with very similar results, etc).');
        $this->l('Hide on mobile');
        $this->l('Hides the slider on mobile devices, including tablets.');
        $this->l('Hide under');
        $this->l('Hides the slider when the viewport width goes under the specified value.');
        $this->l('Hide over');
        $this->l('Hides the slider when the viewport becomes wider than the specified value.');
        $this->l('Use slide effect when swiping');
        $this->l('Ignore selected slide transitions and use sliding effects only when users are changing slides with a swipe gesture on mobile devices.');
        $this->l('Auto-start slideshow');
        $this->l('Slideshow will automatically start after page load.');
        $this->l('Start only in viewport');
        $this->l('The slider will not start until it becomes visible.');
        $this->l('Pause layers');
        $this->l('If you enable this option, layer transitions will not start playing as long the slideshow is in a paused state.');
        $this->l('Pause on hover');
        $this->l('Do nothing');
        $this->l('Pause slideshow');
        $this->l('Pause slideshow and layer transitions');
        $this->l('Pause slideshow and layer transitions, including loops');
        $this->l('Decide what should happen when you move your mouse cursor over the slider.');
        $this->l('Start with slide');
        $this->l('The slider will start with the specified slide. You can also use the value "random".');
        $this->l('Keyboard navigation');
        $this->l('You can navigate through slides with the left and right arrow keys.');
        $this->l('Touch navigation');
        $this->l('Gesture-based navigation when swiping on touch-enabled devices.');
        $this->l('Enable');
        $this->l('Play the slider by scrolling the web page. <a href="https://layerslider.webshopworks.com/play-by-scroll-26" target="_blank">Click here</a> to see a live example.');
        $this->l('Speed');
        $this->l('Play By Scroll speed multiplier.');
        $this->l('Cycles');
        $this->l('Number of cycles if slideshow is enabled.');
        $this->l('Force number of cycles');
        $this->l('The slider will always stop at the given number of cycles, even if the slideshow restarts.');
        $this->l('Shuffle mode');
        $this->l('Slideshow will proceed in random order. This feature does not work with looping.');
        $this->l('Two way slideshow');
        $this->l('Slideshow can go backwards if someone switches to a previous slide.');
        $this->l('Forced animation duration');
        $this->l('The animation speed in milliseconds when the slider forces remaining layers out of scene before swapping slides.');
        $this->l('Skin');
        $this->l('The skin used for this slider. The \'noskin\' skin is a border- and buttonless skin. Your custom skins will appear in the list when you create their folders.');
        $this->l('Initial fade duration');
        $this->l('Change the duration of the initial fade animation when the page loads. Enter 0 to disable fading.');
        $this->l('Slider CSS');
        $this->l('You can enter custom CSS to change some style properties on the slider wrapper element. More complex CSS should be applied with the Custom Styles Editor.');
        $this->l('Background color');
        $this->l('Global background color of the slider. Slides with non-transparent background will cover this one. You can use all CSS methods such as HEX or RGB(A) values.');
        $this->l('Background image');
        $this->l('Global background image of the slider. Slides with non-transparent backgrounds will cover it. This image will not scale in responsive mode.');
        $this->l('Background repeat');
        $this->l('Global background image repeat.');
        $this->l('No-repeat');
        $this->l('Repeat');
        $this->l('Repeat-x');
        $this->l('Repeat-y');
        $this->l('Background behavior');
        $this->l('Choose between a scrollable or fixed global background image.');
        $this->l('Scroll');
        $this->l('Fixed');
        $this->l('Global background image position of the slider. The first value is the horizontal position and the second value is the vertical.');
        $this->l('Global background size of the slider. You can set the size in pixels, percentages, or constants: auto | cover | contain ');
        $this->l('Show Prev & Next buttons');
        $this->l('Disabling this option will hide the Prev and Next buttons.');
        $this->l('Show Prev & Next buttons on hover');
        $this->l('Show the buttons only when someone moves the mouse cursor over the slider. This option depends on the previous setting.');
        $this->l('Show Start & Stop buttons');
        $this->l('Disabling this option will hide the Start & Stop buttons.');
        $this->l('Show slide navigation buttons');
        $this->l('Disabling this option will hide slide navigation buttons or thumbnails.');
        $this->l('Slide navigation on hover');
        $this->l('Slide navigation buttons (including thumbnails) will be shown on mouse hover only.');
        $this->l('Show bar timer');
        $this->l('Show the bar timer to indicate slideshow progression.');
        $this->l('Show circle timer');
        $this->l('Use circle timer to indicate slideshow progression.');
        $this->l('Show slidebar timer');
        $this->l('You can grab the slidebar timer playhead and seek the whole slide real-time like a movie.');
        $this->l('Thumbnail Navigation');
        $this->l('Use thumbnail navigation instead of slide bullet buttons.');
        $this->l('Disabled');
        $this->l('Hover');
        $this->l('Always');
        $this->l('Thumbnail container width');
        $this->l('The width of the thumbnail area relative to the slider size.');
        $this->l('Thumbnail width');
        $this->l('The width of thumbnails in the navigation area.');
        $this->l('Thumbnail height');
        $this->l('The height of thumbnails in the navigation area.');
        $this->l('Active thumbnail opacity');
        $this->l('Opacity in percentage of the active slide\'s thumbnail.');
        $this->l('Inactive thumbnail opacity');
        $this->l('Opacity in percentage of inactive slide thumbnails.');
        $this->l('Automatically play videos');
        $this->l('Videos will be automatically started on the active slide.');
        $this->l('The slideshow can temporally be paused while videos are playing. You can choose to permanently stop the pause until manual restarting.');
        $this->l('While playing');
        $this->l('Permanently');
        $this->l('No action');
        $this->l('Youtube preview');
        $this->l('The automatically fetched preview image quaility for YouTube videos when you do not set your own. Please note, some videos do not have HD previews, and you may need to choose a lower quaility.');
        $this->l('Maximum quality');
        $this->l('High quality');
        $this->l('Medium quality');
        $this->l('Default quality');
        $this->l('Use relative URLs');
        $this->l('Use relative URLs for local images. This setting could be important when moving your PS installation.');
        $this->l('Allow restarting slides on resize');
        $this->l('Certain transformation and transition options cannot be updated on the fly when the browser size or device orientation changes. By enabling this option, the slider will automatically detect such situations and will restart the itself to preserve its appearance.');
        $this->l('Use srcset attribute');
        $this->l('The srcset attribute allows loading dynamically scaled images based on screen resolution. It can save bandwidth and allow using retina-ready images on high resolution devices. In some rare edge cases, this option might cause blurry images.');
        $this->l('YourLogo');
        $this->l('A fixed image layer can be shown above the slider that remains still throughout the whole slider. Can be used to display logos or watermarks.');
        $this->l('YourLogo style');
        $this->l('CSS properties to control the image placement and appearance.');
        $this->l('YourLogo link');
        $this->l('Enter a URL to link the YourLogo image.');
        $this->l('Link Target');
        $this->l('Open on the same page');
        $this->l('Open on new page');
        $this->l('Open in parent frame');
        $this->l('Open in main frame');
        $this->l('Date Created');
        $this->l('Last Modified');
        $this->l('Post ID');
        $this->l('Post Title');
        $this->l('Number of Comments');
        $this->l('Random');
        $this->l('Ascending');
        $this->l('Descending');
        $this->l('Set a slide image');
        $this->l('The slide image/background. Click on the image to open the Image Manager to choose or upload an image.');
        $this->l('Size');
        $this->l('The size of the slide background image. Leave this option on inherit if you want to set it globally from Slider Settings.');
        $this->l('Inherit');
        $this->l('Position');
        $this->l('The position of the slide background image. Leave this option on inherit if you want to set it globally from Slider Settings.');
        $this->l('Color');
        $this->l('The slide background color. You can use color names, hexadecimal, RGB or RGBA values.');
        $this->l('Set a slide thumbnail');
        $this->l('The thumbnail image of this slide. Click on the image to open the Image Manager to choose or upload an image. If you leave this field empty, the slide image will be used.');
        $this->l('Duration');
        $this->l('Here you can set the time interval between slide changes, this slide will stay visible for the time specified here. This value is in millisecs, so the value 1000 means 1 second. Please don\'t use 0 or very low values.');
        $this->l('We\'ve made our pre-defined slide transitions with special care to fit in most use cases. However, if you would like to increase or decrease the speed of these transitions, you can override their timing here by providing your own transition length in milliseconds. (1 second = 1000 milliseconds)');
        $this->l('custom duration');
        $this->l('Time Shift');
        $this->l('You can shift the starting point of the slide animation timeline, so layers can animate in an earlier time after a slide change. This value is in milliseconds. A second is 1000 milliseconds. You can only use a negative value.');
        $this->l('enter URL');
        $this->l('If you want to link the whole slide, enter the URL of your link here.');
        $this->l('Scroll to element (Enter selector)');
        $this->l('#ID');
        $this->l('You can apply an ID attribute on the HTML element of this slide to work with it in your custom CSS or Javascript code.');
        $this->l('Deeplink');
        $this->l('You can specify a slide alias name which you can use in your URLs with a hash mark, so LayerSlider will start with the correspondig slide.');
        $this->l('Hidden');
        $this->l('If you don\'t want to use this slide in your front-page, but you want to keep it, you can hide it with this switch.');
        $this->l('Overflow layers');
        $this->l('By default the slider clips the layers outside of its bounds. Enable this option to allow overflowing content.');
        $this->l('Zoom');
        $this->l('Zoom In');
        $this->l('Zoom Out');
        $this->l('Rotate');
        $this->l('The amount of rotation (if any) in degrees used in the Ken Burns effect. Negative values are allowed for counterclockwise rotation.');
        $this->l('Scale');
        $this->l('Increase or decrease the size of the slide background image in the Ken Burns effect. The default value is 1, the value 2 will double the image, while 0.5 results half the size. Negative values will flip the image.');
        $this->l('Type');
        $this->l('The default value for parallax layers on this slide, which they will inherit, unless you set it otherwise on the affected layers.');
        $this->l('2D');
        $this->l('3D');
        $this->l('Event');
        $this->l('You can trigger the parallax effect by either scrolling the page, or by moving your mouse cursor / tilting your mobile device. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('Cursor or Tilt');
        $this->l('Axes');
        $this->l('Choose on which axes parallax layers should move. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('None');
        $this->l('Both axes');
        $this->l('Horizontal only');
        $this->l('Vertical only');
        $this->l('Transform Origin');
        $this->l('Sets a point on canvas from which transformations are calculated. For example, a layer may rotate around its center axis or a completely custom point, such as one of its corners. The three values represent the X, Y and Z axes in 3D space. Apart from the pixel and percentage values, you can also use the following constants: top, right, bottom, left, center.');
        $this->l('Move Duration');
        $this->l('Controls the speed of animating layers when you move your mouse cursor or tilt your mobile device. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('Leave Duration');
        $this->l('Controls how quickly your layers revert to their original position when you move your mouse cursor outside of a parallax slider. This value is in milliseconds. 1 second = 1000 milliseconds. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('Distance');
        $this->l('Increase or decrease the amount of layer movement when moving your mouse cursor or tilting on a mobile device. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('Rotation');
        $this->l('Increase or decrease the amount of layer rotation in the 3D space when moving your mouse cursor or tilting on a mobile device. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('Perspective');
        $this->l('Changes the perspective of layers in the 3D space. This is the default value on this slide, which parallax layers will inherit, unless you set it otherwise directly on them.');
        $this->l('Filter From');
        $this->l('Filters provide effects like blurring or color shifting your layers. Click into the text field to see a selection of filters you can use. Although clicking on the pre-defined options will reset the text field, you can apply multiple filters simply by providing a space separated list of all the filters you would like to use.');
        $this->l('Filter To');
        $this->l('Autoplay');
        $this->l('Enabled');
        $this->l('Show Info');
        $this->l('Controls');
        $this->l('Fill mode');
        $this->l('Volume');
        $this->l('Use this video as slide background');
        $this->l('Forces this layer to act like the slide background by covering the whole slider and ignoring some transitions. Please make sure to provide your own poster image with the option above, so the slider can display it immediately on page load.');
        $this->l('Choose an overlay image:');
        $this->l('Cover your videos with an overlay image to have dotted or striped effects on them.');
        $this->l('OffsetX');
        $this->l('Shifts the layer starting position from its original on the horizontal axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'left\' or \'right\' position the layer out the staging area, so it enters the scene from either side when animating to its destination location.');
        $this->l('OffsetY');
        $this->l('Shifts the layer starting position from its original on the vertical axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the height of this layer. The values \'top\' or \'bottom\' position the layer out the staging area, so it enters the scene from either vertical side when animating to its destination location.');
        $this->l('The length of the transition in milliseconds when the layer enters the scene. A second equals to 1000 milliseconds.');
        $this->l('Start at');
        $this->l('Delays the transition with the given amount of milliseconds before the layer enters the scene. A second equals to 1000 milliseconds.');
        $this->l('Easing');
        $this->l('The timing function of the animation. With this function you can manipulate the movement of the animated object. Please click on the link next to this select field to open easings.net for more information and real-time examples.');
        $this->l('Fade');
        $this->l('Fade the layer during the transition.');
        $this->l('Rotates the layer by the given number of degrees. Negative values are allowed for counterclockwise rotation.');
        $this->l('RotateX');
        $this->l('Rotates the layer along the X (horizontal) axis by the given number of degrees. Negative values are allowed for reverse direction.');
        $this->l('RotateY');
        $this->l('Rotates the layer along the Y (vertical) axis by the given number of degrees. Negative values are allowed for reverse direction.');
        $this->l('SkewX');
        $this->l('Skews the layer along the X (horizontal) by the given number of degrees. Negative values are allowed for reverse direction.');
        $this->l('SkewY');
        $this->l('Skews the layer along the Y (vertical) by the given number of degrees. Negative values are allowed for reverse direction.');
        $this->l('ScaleX');
        $this->l('Scales the layer along the X (horizontal) axis by the specified vector. Use the value 1 for the original size. The value 2 will double, while 0.5 shrinks the layer compared to its original size.');
        $this->l('ScaleY');
        $this->l('Scales the layer along the Y (vertical) axis by the specified vector. Use the value 1 for the original size. The value 2 will double, while 0.5 shrinks the layer compared to its original size.');
        $this->l('Sets a point on canvas from which transformations are calculated. For example, a layer may rotate around its center axis or a completely custom point, such as one of its corners. The three values represent the X, Y and Z axes in 3D space. Apart from the pixel and percentage values, you can also use the following constants: top, right, bottom, left, center, slidercenter, slidermiddle, slidertop, sliderright, sliderbottom, sliderleft.');
        $this->l('Mask');
        $this->l('Clips (cuts off) the sides of the layer by the given amount specified in pixels or percentages. The 4 value in order: top, right, bottom and the left side of the layer.');
        $this->l('Background');
        $this->l('The background color of your layer. You can use color names, hexadecimal, RGB or RGBA values as well as the \'transparent\' keyword. Example: #FFF');
        $this->l('The color of your text. You can use color names, hexadecimal, RGB or RGBA values. Example: #333');
        $this->l('Rounded corners');
        $this->l('If you want rounded corners, you can set its radius here in pixels. Example: 5px');
        $this->l('Width');
        $this->l('The initial width of this layer from which it will be animated to its proper size during the transition.');
        $this->l('Height');
        $this->l('The initial height of this layer from which it will be animated to its proper size during the transition.');
        $this->l('Filter');
        $this->l('Filters provide effects like blurring or color shifting your layers. Click into the text field to see a selection of filters you can use. Although clicking on the pre-defined options will reset the text field, you can apply multiple filters simply by providing a space separated list of all the filters you would like to use. Click on the "Filter" link for more information.');
        $this->l('Changes the perspective of this layer in the 3D space.');
        $this->l('Shifts the layer from its original position on the horizontal axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'left\' or \'right\' animate the layer out the staging area, so it can leave the scene on either side.');
        $this->l('Shifts the layer from its original position on the vertical axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the height of this layer. The values \'top\' or \'bottom\' animate the layer out the staging area, so it can leave the scene on either vertical side.');
        $this->l('The length of the transition in milliseconds when the layer leaves the slide. A second equals to 1000 milliseconds.');
        $this->l('You can set the starting time of this transition. Use one of the pre-defined options to use relative timing, which can be shifted with custom operations.');
        $this->l('Slide change starts (ignoring modifier)');
        $this->l('Opening Transition completes');
        $this->l('Opening Text Transition starts');
        $this->l('Opening Text Transition completes');
        $this->l('Opening and Opening Text Transition complete');
        $this->l('Loop starts');
        $this->l('Loop completes');
        $this->l('Opening and Loop Transitions complete');
        $this->l('Opening Text and Loop Transitions complete');
        $this->l('Opening, Opening Text and Loop Transitions complete');
        $this->l('Ending Text Transition starts');
        $this->l('Ending Text Transition completes');
        $this->l('Ending Text and Loop Transitions complete');
        $this->l('Skews the layer along the X (horizontal) axis by the given number of degrees. Negative values are allowed for reverse direction.');
        $this->l('Skews the layer along the Y (vertical) axis by the given number of degrees. Negative values are allowed for reverse direction.');
        $this->l('If you don\'t want to use this layer, but you want to keep it, you can hide it with this switch.');
        $this->l('Animates the background toward the color you specify here when the layer leaves the slider canvas.');
        $this->l('Animates the text color toward the color you specify here when the layer leaves the slider canvas.');
        $this->l('Animates rounded corners toward the value you specify here when the layer leaves the slider canvas.');
        $this->l('Animates the layer width toward the value you specify here when the layer leaves the slider canvas.');
        $this->l('Animates the layer height toward the value you specify here when the layer leaves the slider canvas.');
        $this->l('Animate');
        $this->l('Select how your text should be split and animated.');
        $this->l('by lines ascending');
        $this->l('by lines descending');
        $this->l('by lines random');
        $this->l('by lines center to edge');
        $this->l('by lines edge to center');
        $this->l('by words ascending');
        $this->l('by words descending');
        $this->l('by words random');
        $this->l('by words center to edge');
        $this->l('by words edge to center');
        $this->l('by chars ascending');
        $this->l('by chars descending');
        $this->l('by chars random');
        $this->l('by chars center to edge');
        $this->l('by chars edge to center');
        $this->l('Shift In');
        $this->l('Delays the transition of each text nodes relative to each other. A second equals to 1000 milliseconds.');
        $this->l('Shifts the starting position of text nodes from their original on the horizontal axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'left\' or \'right\' position text nodes out the staging area, so they enter the scene from either side when animating to their destination location. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Shifts the starting position of text nodes from their original on the vertical axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'top\' or \'bottom\' position text nodes out the staging area, so they enter the scene from either vertical side when animating to their destination location. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('The transition length in milliseconds of the individual text fragments. A second equals to 1000 milliseconds.');
        $this->l('The timing function of the animation. With this function you can manipulate the movement of animated text fragments. Please click on the link next to this select field to open easings.net for more information and real-time examples.');
        $this->l('Fade the text fragments during their transition.');
        $this->l('StartAt');
        $this->l('Opening Transition starts');
        $this->l('Rotates text fragments clockwise by the given number of degrees. Negative values are allowed for counterclockwise rotation. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Rotates text fragments along the X (horizontal) axis by the given number of degrees. Negative values are allowed for reverse direction. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Rotates text fragments along the Y (vertical) axis by the given number of degrees. Negative values are allowed for reverse direction. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Scales text fragments along the X (horizontal) axis by the specified vector. Use the value 1 for the original size. The value 2 will double, while 0.5 shrinks text fragments compared to their original size. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Scales text fragments along the Y (vertical) axis by the specified vector. Use the value 1 for the original size. The value 2 will double, while 0.5 shrinks text fragments compared to their original size. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Skews text fragments along the X (horizontal) axis by the given number of degrees. Negative values are allowed for reverse direction. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Skews text fragments along the Y (vertical) axis by the given number of degrees. Negative values are allowed for reverse direction. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Shift Out');
        $this->l('Shifts the ending position of text nodes from their original on the horizontal axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'left\' or \'right\' position text nodes out the staging area, so they leave the scene from either side when animating to their destination location. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Shifts the ending position of text nodes from their original on the vertical axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'top\' or \'bottom\' position text nodes out the staging area, so they leave the scene from either vertical side when animating to their destination location. By listing multiple values separated with a | character, the slider will use different transition variations on each text node by cycling between the provided values.');
        $this->l('Shifts the layer starting position from its original on the horizontal axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. The values \'left\' or \'right\' position the layer out the staging area, so it can leave and re-enter the scene from either side during the transition.');
        $this->l('Shifts the layer starting position from its original on the vertical axis with the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the height of this layer. The values \'top\' or \'bottom\' position the layer out the staging area, so it can leave and re-enter the scene from either vertical side during the transition.');
        $this->l('The length of the transition in milliseconds. A second is equal to 1000 milliseconds.');
        $this->l('The timing function of the animation to manipualte the layer\'s movement. Click on the link next to this field to open easings.net for examples and more information');
        $this->l('Opacity');
        $this->l('Fades the layer. You can use values between 1 and 0 to set the layer fully opaque or transparent respectively. For example, the value 0.5 will make the layer semi-transparent.');
        $this->l('Count');
        $this->l('The number of times repeating the Loop transition. The count includes the reverse part of the transitions when you use the Yoyo feature. Use the value -1 to repeat infinitely or zero to disable looping.');
        $this->l('Wait');
        $this->l('Waiting time between repeats in milliseconds. A second is 1000 milliseconds.');
        $this->l('Yoyo');
        $this->l('Enable this option to allow reverse transition, so you can loop back and forth seamlessly.');
        $this->l('Moves the layer horizontally by the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. ');
        $this->l('Moves the layer vertically by the given number of pixels. Use negative values for the opposite direction. Percentage values are relative to the width of this layer. ');
        $this->l('Reverse<br>duration');
        $this->l('The duration of the reverse transition in milliseconds. A second is equal to 1000 milliseconds.');
        $this->l('Reverse<br>easing');
        $this->l('The timing function of the reverse animation to manipualte the layer\'s movement. Click on the link next to this field to open easings.net for examples and more information');
        $this->l('Rotates the layer clockwise by the given number of degrees. Negative values are allowed for counterclockwise rotation.');
        $this->l('The background color of this layer. You can use color names, hexadecimal, RGB or RGBA values as well as the \'transparent\' keyword. Example: #FFF');
        $this->l('The text color of this text. You can use color names, hexadecimal, RGB or RGBA values. Example: #333');
        $this->l('If you want rounded corners, you can set here its radius in pixels. Example: 5px');
        $this->l('Changes the perspective of layers in the 3D space.');
        $this->l('Always on top');
        $this->l('Show this layer above every other layer while hovering.');
        $this->l('Parallax Level');
        $this->l('Set the intensity of the parallax effect. Use negative values to shift layers in the opposite direction.');
        $this->l('Choose if you want 2D or 3D parallax layers.');
        $this->l('Inherit from Slide Options');
        $this->l('You can trigger the parallax effect by either scrolling the page, or by moving your mouse cursor / tilting your mobile device.');
        $this->l('Choose on which axes parallax layers should move.');
        $this->l('Both');
        $this->l('Controls the speed of animating layers when you move your mouse cursor or tilt your mobile device.');
        $this->l('Controls how quickly parallax layers revert to their original position when you move your mouse cursor outside of the slider. This value is in milliseconds. A second equals to 1000 milliseconds.');
        $this->l('Increase or decrease the amount of layer rotation in the 3D space when moving your mouse cursor or tilting on a mobile device.');
        $this->l('Increase or decrease the amount of layer movement when moving your mouse cursor or tilting on a mobile device.');
        $this->l('Keep this layer visible:');
        $this->l('You can keep this layer on top of the slider across multiple slides. Just select the slide on which this layer should animate out. Alternatively, you can make this layer global on all slides after it transitioned in.');
        $this->l('Until the end of this slide (default)');
        $this->l('Forever (the layer will never animate out)');
        $this->l('Play By Scroll Keyframe');
        $this->l('A Play by Scroll slider will pause when this layer finished its opening transition.');
        $this->l('If you want to link your layer, type here the URL. You can use a hash mark followed by a number to link this layer to another slide. Example: #3 - this will switch to the third slide.');
        $this->l('URL target');
        $this->l('Custom Attributes');
        $this->l('Your list of custom attributes. Use this feature if your needs are not covered by the common attributes above or you want to override them. You can use data-* as well as regular attribute names. Empty attributes (without value) are also allowed. For example, to make a FancyBox gallery, you may enter "data-fancybox-group" and "gallery1" for the attribute name and value, respectively.');
        $this->l('You can set the width of your layer. You can use pixels, percentage, or the default value \'auto\'. Examples: 100px, 50% or auto.');
        $this->l('You can set the height of your layer. You can use pixels, percentage, or the default value \'auto\'. Examples: 100px, 50% or auto');
        $this->l('Top');
        $this->l('The layer position from the top of the slide. You can use pixels and percentage. Examples: 100px or 50%. You can move your layers in the preview above with a drag n\' drop, or set the exact values here.');
        $this->l('Left');
        $this->l('The layer position from the left side of the slide. You can use pixels and percentage. Examples: 100px or 50%. You can move your layers in the preview above with a drag n\' drop, or set the exact values here.');
        $this->l('Padding on the top of the layer. Example: 10px');
        $this->l('Right');
        $this->l('Padding on the right side of the layer. Example: 10px');
        $this->l('Bottom');
        $this->l('Padding on the bottom of the layer. Example: 10px');
        $this->l('Padding on the left side of the layer. Example: 10px');
        $this->l('Border on the top of the layer. Example: 5px solid #000');
        $this->l('Border on the right side of the layer. Example: 5px solid #000');
        $this->l('Border on the bottom of the layer. Example: 5px solid #000');
        $this->l('Border on the left side of the layer. Example: 5px solid #000');
        $this->l('Family');
        $this->l('List of your chosen fonts separated with a comma. Please use apostrophes if your font names contains white spaces. Example: Helvetica, Arial, sans-serif');
        $this->l('Font size');
        $this->l('The font size in pixels. Example: 16px.');
        $this->l('Line height');
        $this->l('The line height of your text. The default setting is \'normal\'. Example: 22px');
        $this->l('Font weight');
        $this->l('100 (UltraLight)');
        $this->l('200 (Thin)');
        $this->l('300 (Light)');
        $this->l('400 (Regular)');
        $this->l('500 (Medium)');
        $this->l('600 (Semibold)');
        $this->l('700 (Bold)');
        $this->l('800 (Heavy)');
        $this->l('900 (Black)');
        $this->l('Font style');
        $this->l('Italic');
        $this->l('Oblique (Forced slant)');
        $this->l('Text decoration');
        $this->l('Underline');
        $this->l('Overline');
        $this->l('Line through');
        $this->l('Text align');
        $this->l('Initial (Language default)');
        $this->l('Center');
        $this->l('Justify');
        $this->l('Min. font size');
        $this->l('The minimum font size in a responsive slider. This option allows you to prevent your texts layers becoming too small on smaller screens.');
        $this->l('Min. mobile font size');
        $this->l('The minimum font size in a responsive slider on mobile devices. This option allows you to prevent your texts layers becoming too small on smaller screens.');
        $this->l('If you want rounded corners, you can set its radius here. Example: 5px');
        $this->l('Custom styles');
        $this->l('If you want to set style settings other than above, you can use here any CSS codes. Please make sure to write valid markup.');
        $this->l('The rotation angle where this layer animates toward when entering into the slider canvas. Negative values are allowed for counterclockwise rotation.');
        $this->l('The rotation angle on the horizontal axis where this animates toward when entering into the slider canvas. Negative values are allowed for reversed direction.');
        $this->l('The rotation angle on the vertical axis where this layer animates toward when entering into the slider canvas. Negative values are allowed for reversed direction.');
        $this->l('The layer horizontal scale where this layer animates toward when entering into the slider canvas.');
        $this->l('The layer vertical scale where this layer animates toward when entering into the slider canvas.');
        $this->l('The layer horizontal skewing angle where this layer animates toward when entering into the slider canvas.');
        $this->l('The layer vertical skewing angle where this layer animates toward when entering into the slider canvas.');
        $this->l('Calculate positions from');
        $this->l('Sets the layer position origin from which top and left values are calculated. The default is the upper left corner of the slider canvas. In a full width and full size slider, your content is centered based on the screen size to achieve the best possible fit. By selecting the "sides of the screen" option in those scenarios, you can allow layers to escape the centered inner area and stick to the sides of the screen.');
        $this->l('sides of the slider');
        $this->l('sides of the screen');
        $this->l('Stacking order');
        $this->l('This option controls the vertical stacking order of layers that overlap. In CSS, it\'s commonly called as z-index. Elements with a higher value are stacked in front of elements with a lower one, effectively covering them. By default, this value is calculated automatically based on the order of your layers, thus simply re-ordering them can fix overlap issues. Use this option only if you want to set your own value manually in special cases like using static layers.<br><br>On each slide, the stacking order starts counting from 100. Providing a number less than 100 will put the layer behind every other layer on all slides. Specifying a much greater number, for example 500, will make the layer to be on top of everything else.');
        $this->l('ID');
        $this->l('You can apply an ID attribute on the HTML element of this layer to work with it in your custom CSS or Javascript code.');
        $this->l('Classes');
        $this->l('You can apply classes on the HTML element of this layer to work with it in your custom CSS or Javascript code.');
        $this->l('Title');
        $this->l('You can add a title to this layer which will display as a tooltip if someone holds his mouse cursor over the layer.');
        $this->l('Alt');
        $this->l('Name or describe your image layer, so search engines and VoiceOver softwares can properly identify it.');
        $this->l('Rel');
        $this->l('Plugins and search engines may use this attribute to get more information about the role and behavior of a link.');
        $this->l('Sets the font boldness. Please note, not every font supports all the listed variants, thus some settings may have the same result.');
        $this->l('Oblique is an auto-generated italic version of your chosen font and can force slating even if there is no italic font variant available. However, you should use the regular italic option whenever is possible. Please double check to load italic font variants when using Google Fonts.');
        $this->l('Advanced option');
        $this->l('Premium feature. Click to learn more.');
        $this->l('Preview');
        $this->l('Tiles');
        $this->l('Rows');
        $this->l('<i>number</i> or <i>min,max</i> If you specify a value greater than 1, LayerSlider will cut your slide into tiles. You can specify here how many rows of your transition should have. If you specify two numbers separated with a comma, LayerSlider will use that as a range and pick a random number between your values.');
        $this->l('Cols');
        $this->l('<i>number</i> or <i>min,max</i> If you specify a value greater than 1, LayerSlider will cut your slide into tiles. You can specify here how many columns of your transition should have. If you specify two numbers separated with a comma, LayerSlider will use that as a range and pick a random number between your values.');
        $this->l('Delay');
        $this->l('You can apply a delay between the tiles and postpone their animation relative to each other.');
        $this->l('Sequence');
        $this->l('You can control the animation order of the tiles here.');
        $this->l('Forward');
        $this->l('Reverse');
        $this->l('Col-forward');
        $this->l('Col-reverse');
        $this->l('Transition');
        $this->l('The duration of the animation. This value is in millisecs, so the value 1000 measn 1 second.');
        $this->l('The type of the animation, either slide, fade or both (mixed).');
        $this->l('Slide');
        $this->l('Mixed');
        $this->l('Direction');
        $this->l('The direction of the slide or mixed animation if you\'ve chosen this type in the previous settings.');
        $this->l('Top left');
        $this->l('Top right');
        $this->l('Bottom left');
        $this->l('Bottom right');
        $this->l('RotateZ');
        $this->l('Depth');
        $this->l('The script tries to identify the optimal depth for your rotated objects (tiles). With this option you can force your objects to have a large depth when performing 180 degree (and its multiplies) rotation.');
        $this->l('Large depth');
        $this->l('Before animation');
        $this->l('The duration of your animation. This value is in millisecs, so the value 1000 means 1 second.');
        $this->l('Add new');
        $this->l('Scale3D');
        $this->l('Animation');
        $this->l('The direction of rotation.');
        $this->l('Vertical');
        $this->l('Horizontal');
        $this->l('After animation');
        $this->l('Name your new slider');
        $this->l('e.g. Homepage slider');
        $this->l('Add slider');
        $this->l('Using beta version');
        $this->l('Send feedback');
        $this->l('Embed Slider');
        $this->l('Easiest Method: Module Position');
        $this->l('This is the most commonly used method. Just select a hook from the list, and it will appear on your frontoffice. (This can be also adjusted in the Slider Settings / Layout tab.)');
        $this->l('In Slider Settings / Misc tab you can also define on which Pages and Categories will it appear. There are additional filters like Shop and Language as well.');
        $this->l('Alternate Method: Shortcode');
        $this->l('Shortcodes can be inserted into content, like CMS page, category or product description. This can help you to place a slider into a suitable area, even if there is no available hook there. To use that is quite easy, just need to place the shortcode into the content editor, where number is the ID of the slider (you can also use slug there if you set that in the slider settings).');
        $this->l('We also have a video tutorial about how easy to embed LayerSlider:');
        $this->l('Tutorial Video');
        $this->l('Activate your site to access premium templates.');
        $this->l('This template is only available for activated sites. Please review the PRODUCT ACTIVATION section on the main LayerSlider screen or <a href="https://support.kreaturamedia.com/docs/layersliderwp/documentation.html#activation" target="_blank">click here</a> for more information.');
        $this->l('Plugin update required');
        $this->l('This slider template requires a newer version of LayerSlider in order to work properly. This is due to additional features introduced in a later version than you have. For updating instructions, please refer to our <a href="https://support.kreaturamedia.com/docs/layersliderwp/documentation.html#updating" target="_blank">online documnetation</a>.');
        $this->l('LayerSlider');
        $this->l('Templates');
        $this->l('sliders');
        $this->l('Plugins');
        $this->l('Skins');
        $this->l('Last updated: ');
        $this->l(' ago');
        $this->l('Just now');
        $this->l('Force Library Update');
        $this->l('All');
        $this->l('All free');
        $this->l('All Premium');
        $this->l('Bundled');
        $this->l('New');
        $this->l('Full width');
        $this->l('Full size');
        $this->l('Landing Page');
        $this->l('Parallax');
        $this->l('Loop');
        $this->l('Text Transition');
        $this->l('Ken Burns');
        $this->l('Play By Scroll');
        $this->l('Filter Transition');
        $this->l('Carousel');
        $this->l('Media');
        $this->l('Experimental');
        $this->l('Special Effects');
        $this->l('3D Transition');
        $this->l('API');
        $this->l('Import');
        $this->l('Coming soon,<br>stay tuned!');
        $this->l('Hide layer in the editor.');
        $this->l('Prevent layer from dragging in the editor.');
        $this->l('Duplicate this layer');
        $this->l('Remove this layer');
        $this->l('Show this layer on the following devices:');
        $this->l('Image');
        $this->l('Enter text only content here ...');
        $this->l('Text');
        $this->l('Enter custom HTML code, which will appear on your front-office pages ...');
        $this->l('HTML');
        $this->l('Paste embed code here   or   add self-hosted media ...');
        $this->l('Video / Audio');
        $this->l('You can enter both post placeholders and custom content here (including HTML) ...');
        $this->l('Dynamic content');
        $this->l('Paragraph');
        $this->l('H1');
        $this->l('H2');
        $this->l('H3');
        $this->l('H4');
        $this->l('H5');
        $this->l('H6');
        $this->l('Click on the image preview to open Image Manager or');
        $this->l('insert from URL');
        $this->l('use product img');
        $this->l('Add Media');
        $this->l('Insert a video poster image from your Image Manager or ');
        $this->l('Please note, the slide background image (if any) will cover the video.');
        $this->l('options');
        $this->l('Click on one or more post placeholders to insert them into your layer\'s content. Post placeholders act like shortcodes in WP, and they will be filled with the actual content from your posts.');
        $this->l('Limit text length (if any)');
        $this->l('Configure dynamic content');
        $this->l('Opening Transition properties');
        $this->l('Opening Text Transition properties');
        $this->l('Loop or Middle Transition properties');
        $this->l('Ending Text Transition properties');
        $this->l('Ending Transition properties');
        $this->l('Hover Transition properties');
        $this->l('Parallax Transition properties');
        $this->l('Opening<br>Transition');
        $this->l('Opening Text<br>Transition');
        $this->l('Loop or Middle<br>Transition');
        $this->l('Ending Text<br>Transition');
        $this->l('Ending<br>Transition');
        $this->l('Hover<br>Transition');
        $this->l('Parallax<br>Transition');
        $this->l('The following are the initial options from which this layer animates toward the appropriate values set under the Styles tab when it enters into the slider canvas.');
        $this->l('Copy transition properties');
        $this->l('Paste transition properties');
        $this->l('Position &amp; Dimensions');
        $this->l('Rotation, Skew &amp; Mask');
        $this->l('Timing &amp; Transform');
        $this->l('Style properties');
        $this->l('The following options specify the initial state of each text fragments before they start animating toward the joint whole word.');
        $this->l('Type, Position &amp; Dimensions');
        $this->l('Rotation &amp; Skew');
        $this->l('Sets the starting time for this transition. Select one of the pre-defined options from this list to control timing in relation with other transition types. Additionally, you can shift starting time with the modifier controls below.');
        $this->l('Start when');
        $this->l('Shifts the above selected starting time by performing a custom operation. For example, &quot;- 1000&quot; will advance the animation by playing it 1 second (1000 milliseconds) earlier.');
        $this->l('with modifier');
        $this->l('Loop / Middle Transition properties');
        $this->l('Repeats a transition based on the options below. If you set the Loop Count to 1, it can also act as a middle transition in the chain of animation lifecycles.');
        $this->l('Each text fragment will animate from the joint whole word to the options you specify here.');
        $this->l('The following options will be the end values where this layer animates toward when it leaves the slider canvas.');
        $this->l('Plays a transition based on the options below when the user moves the mouse cursor over this layer.');
        $this->l('Select a parallax type and event, then set the Parallax Level option to enable parallax layers.');
        $this->l('Basic Settings');
        $this->l('Distance &amp; Rotation');
        $this->l('Other settings');
        $this->l('Linking');
        $this->l('or');
        $this->l('use product URL');
        $this->l('Common Attributes');
        $this->l('In some cases your layers may be wrapped by another element. For example, an ＜A＞ tag when you use layer linking. Some attributes will be applied on the wrapper (if any), which is desirable in many cases (e.g. lightbox plugins). If there is no wrapper element, attributes will be automatically applied on the layer itself. If the pre-defined option doesn\'t fit your needs, use custom attributes below to override it.');
        $this->l('On layer');
        $this->l('On parent');
        $this->l('Attribute name');
        $this->l('Attribute value');
        $this->l('In some cases your layers may be wrapped by another element. For example, an ＜A＞ tag when you use layer linking. By default, new attributes will be applied on the wrapper (if any), which is desirable in most cases (e.g. lightbox plugins). If there is no wrapper element, attributes will be automatically applied on the layer itself. Uncheck this option when you need to apply this attribute on the layer element in all cases.');
        $this->l('Layout');
        $this->l('sizing & position');
        $this->l('Border');
        $this->l('Padding');
        $this->l('Transforms');
        $this->l('between transitions');
        $this->l('Actions');
        $this->l('Copy layer styles');
        $this->l('Paste layer styles');
        $this->l('font &amp; style');
        $this->l('Word-wrap');
        $this->l('Misc');
        $this->l('Custom CSS');
        $this->l('write your own code');
        $this->l('If you want to set style settings other then above, you can use here any CSS codes. Please make sure to write valid markup.');
        $this->l('Find products with the filters above');
        $this->l('Advanced');
        $this->l('New Arrivals');
        $this->l('Popular');
        $this->l('Best Sellers');
        $this->l('Special');
        $this->l('Don\'t filter categories');
        $this->l('Don\'t filter tags');
        $this->l('Don\'t filter taxonomies');
        $this->l('Order results by');
        $this->l('On this slide');
        $this->l('Get the ');
        $this->l('following');
        $this->l('item in the set of matched selection');
        $this->l('Preview from currenty matched elements');
        $this->l('Enjoy using LayerSlider?');
        $this->l('If so, please consider recommending it to your friends on your favorite social network!');
        $this->l('Share');
        $this->l('Tweet');
        $this->l('Slide Options');
        $this->l('Duplicate slide');
        $this->l('Slide Background Image');
        $this->l('|');
        $this->l('Slide Thumbnail');
        $this->l('Slide Timing');
        $this->l('Slide Transition');
        $this->l('You can select your desired slide transitions by clicking on this button.');
        $this->l('Slide Linking');
        $this->l('Configure<br>dynamic content');
        $this->l('Additional Slide Settings');
        $this->l('Hide this slide');
        $this->l('Ken Burns Effect');
        $this->l('Parallax defaults');
        $this->l('Filters');
        $this->l('Show More Options');
        $this->l('Linking, Ken Burns, Parallax');
        $this->l('Show Less Options');
        $this->l('Auto-Fit');
        $this->l('Align Layer to...');
        $this->l('top center');
        $this->l('middle left');
        $this->l('middle center');
        $this->l('middle right');
        $this->l('bottom center');
        $this->l('Undo');
        $this->l('Redo');
        $this->l('Copy...');
        $this->l('Paste...');
        $this->l('Layer');
        $this->l('Show layers that are visible on desktop.');
        $this->l('Show layers that are visible on tablets.');
        $this->l('Show layers that are visible on mobile phones.');
        $this->l('Drop image(s) here');
        $this->l('Layers');
        $this->l('Layer options');
        $this->l('Timeline');
        $this->l('Static layers from other slides');
        $this->l('Layers on this slide');
        $this->l('Layer editor');
        $this->l('Put back');
        $this->l('Multiple selection');
        $this->l('Editing multiple layers feature is coming soon.');
        $this->l('Content');
        $this->l('Transitions');
        $this->l('Link & Attributes');
        $this->l('Styles');
        $this->l('Pop out editor');
        $this->l('Type your slider name here');
        $this->l('Slider slug');
        $this->l('e.g. homepageslider');
        $this->l('Slider Settings');
        $this->l('Show advanced settings');
        $this->l('Publish');
        $this->l('Mobile');
        $this->l('Slideshow');
        $this->l('Appearance');
        $this->l('Navigation Area');
        $this->l('Videos');
        $this->l('Default Options');
        $this->l('Interpreted as:');
        $this->l('Slider type & dimensions');
        $this->l('Fixed size');
        $this->l('Responsive');
        $this->l('Slideshow behavior');
        $this->l('Slideshow navigation');
        $this->l('Slider appearance');
        $this->l('Custom slider CSS');
        $this->l('Slider global background');
        $this->l('Show navigation buttons');
        $this->l('Navigation buttons on hover');
        $this->l('Slideshow timers');
        $this->l('Thumbnail dimensions');
        $this->l('Thumbnail appearance');
        $this->l('Slide background defaults');
        $this->l('Slider preview image');
        $this->l('The preview image you can see in your list of sliders.');
        $this->l('Click this icon to jump to the slide where this layer was added on, so you can quickly edit its settings.');
        $this->l('Choose a slide transition to import');
        $this->l('Show transitions:');
        $this->l('Select slide transitions');
        $this->l('Custom 2D &amp; 3D');
        $this->l('Apply to others');
        $this->l('Select all');
        $this->l('Custom 2D transitions');
        $this->l('You haven\'t created any custom 2D transitions yet.');
        $this->l('Custom 3D transitions');
        $this->l('You haven\'t created any custom 3D transitions yet.');
        $this->l('Special effects are like regular slide transitions and they work in the same way. You can set them on each slide individually. Mixing them with other transitions on other slides is perfectly fine. You can also apply them on all of your slides at once by pressing the "Apply to others" button above. In case of 3D special effects, selecting additional 2D transitions can ensure backward compatibility for older browsers.');
        $this->l('Origami transition');
        $this->l('Share your gorgeous photos with the world or your loved ones in a truly inspirational way and create sliders with stunning effects with Origami.');
        $this->l('Origami is a form of 3D transition and it works in the same way as regular slide transitions do. Besides Internet Explorer, Origami works in all the modern browsers (including Edge).');
        $this->l('Use it on this slide');
        $this->l('Click here for live example');
        $this->l('More effects are coming soon');
        $this->l('Upload Slider');
        $this->l('Here you can upload your previously exported sliders. To import them to your site, you just need to choose and select the appropriate export file (files with .zip or .json extensions), then press the Import Sliders button.');
        $this->l('Looking for the importable demo content? Check out the <a href="#" class="ls-open-template-store" data-delay="750"><i class="dashicons dashicons-star-filled"></i>Template Store</a>.');
        $this->l('Notice: In order to import from outdated versions (pre-v3.0.0), you need to create a new file and paste the export code into it. The file needs to have a .json extension, then you will be able to upload it.');
        $this->l('Import Sliders');
        $this->l('Show on screen');
        $this->l('Tooltips');
        $this->l('Screen Options');
        $this->l('LayerSlider Skin Editor');
        $this->l('Back to the list');
        $this->l('Your changes has been saved!');
        $this->l('Skin Editor');
        $this->l('Ctrl+Q to fold/unfold a block');
        $this->l('Choose a skin:');
        $this->l('Built-in skins will be overwritten by plugin updates. Making changes should be done through the Custom Styles Editor.');
        $this->l('You need to make this file writable in order to save your changes.');
        $this->l('Save changes');
        $this->l('Modifying a skin with invalid code can break your sliders\' appearance. Changes cannot be reverted after saving.');
        $this->l('Use features');
        $this->l('Editing slider:');
        $this->l('Slides');
        $this->l('Event Callbacks');
        $this->l('FAQ');
        $this->l('Documentation');
        $this->l('Need help? Try these: ');
        $this->l('Add new slide');
        $this->l('Please read our <a href="https://support.kreaturamedia.com/docs/layersliderwp/documentation.html#layerslider-api" target="_blank">online documentation</a> before start using the API. LayerSlider 6 introduced an entirely new API model with different events and methods.');
        $this->l('Fires before parsing user settings and rendering the UI.');
        $this->l('Fires when the slider is fully initialized and its DOM nodes become accessible.');
        $this->l('Fires before the slider renders resize events.');
        $this->l('Fires after the slider has rendered resize events.');
        $this->l('Fires upon every slideshow state change, which may not influence the playing status.');
        $this->l('Fires when the slideshow pauses from playing status.');
        $this->l('Fires when the slideshow resumes from paused status.');
        $this->l('Signals when the slider wants to change slides, and is your last chance to divert it or intervene in any way.');
        $this->l('Fires when the slider has started a slide change.');
        $this->l('Fires before completing a slide change.');
        $this->l('Fires after a slide change has completed and the slide indexes have been updated. ');
        $this->l('Fires when the current slide\'s animation timeline (e.g. your layers) becomes accessible for interfacing.');
        $this->l('Fires rapidly (at each frame) throughout the entire slide while playing, including reverse playback.');
        $this->l('Fires when the current slide\'s animation timeline (e.g. your layers) has started playing.');
        $this->l('Fires when the current slide\'s animation timeline (e.g. layer transitions) has completed.');
        $this->l('Fires when all reversed animations have reached the beginning of the current slide.');
        $this->l('Fires when the slider destructor has finished and it is safe to remove the slider from the DOM.');
        $this->l('Fires when the slider has been removed from the DOM when using the <i>destroy</i> API method.');
        $this->l('The events below were used in version 5 and earlier. These events are no longer in use, they cannot be edited. They are shown only to offer you a way of viewing and porting them to the new API.');
        $this->l('Fires when LayerSlider has loaded.');
        $this->l('Calling when the slideshow has started.');
        $this->l('Calling when the slideshow is stopped by the user.');
        $this->l('Fireing when the slideshow is temporary on hold (e.g.: "Pause on hover" feature).');
        $this->l('Calling when the slider commencing slide change (animation start).');
        $this->l('Fireing when the slider finished a slide change (animation end).');
        $this->l('Calling when the slider will change to the previous slide by the user.');
        $this->l('Calling when the slider will change to the next slide by the user.');
        $this->l('Use shortcode:');
        $this->l('Successfully emptied LayerSlider caches.');
        $this->l('Successfully updated the Template Store library.');
        $this->l('No sliders were selected to remove.');
        $this->l('The selected sliders were removed.');
        $this->l('The selected sliders were duplicated.');
        $this->l('No sliders were selected.');
        $this->l('The selected sliders were permanently deleted.');
        $this->l('You need to select at least 2 sliders to merge them.');
        $this->l('The selected items were merged together as a new slider.');
        $this->l('The selected sliders were restored.');
        $this->l('No sliders were found to export.');
        $this->l('No sliders were selected to export.');
        $this->l('The PHP ZipArchive extension is required to import .zip files.');
        $this->l('Choose a file to import sliders.');
        $this->l('The import file seems to be invalid or corrupted.');
        $this->l('Your slider has been imported.');
        $this->l('Your account does not have the necessary permission you have chosen, and your settings have not been saved in order to prevent locking yourself out of the plugin.');
        $this->l('Permission changes has been updated.');
        $this->l('Your Google Fonts library has been updated.');
        $this->l('Your settings has been updated.');
        $this->l('Show me');
        $this->l('sliders per page');
        $this->l('Apply');
        $this->l('Interactive guides coming soon!');
        $this->l('Interactive step-by-step tutorial guides will shortly arrive to help you get started using LayerSlider.');
        $this->l('Guides');
        $this->l('Your Sliders');
        $this->l('List View');
        $this->l('Grid View');
        $this->l('Show');
        $this->l('published');
        $this->l('Sort by');
        $this->l('Name');
        $this->l('date modified');
        $this->l('date scheduled');
        $this->l('Filter by name');
        $this->l('Search');
        $this->l('Template Store');
        $this->l('Add New Slider');
        $this->l('Slider preview');
        $this->l('Shortcode');
        $this->l('Created');
        $this->l('Modified');
        $this->l('ago');
        $this->l('Restore removed slider');
        $this->l('Export');
        $this->l('Duplicate');
        $this->l('Remove');
        $this->l('No Preview');
        $this->l('Previews are automatically generated from slide images in sliders.');
        $this->l('No sliders found with the current filters set. <a href="?page=layerslider">Click here</a> to reset filters.');
        $this->l('Add a new slider or check out the <a href="#" class="ls-open-template-store"><i class="dashicons dashicons-star-filled"></i>Template Store</a> to get started using LayerSlider.');
        $this->l('Bulk Actions');
        $this->l('Export selected');
        $this->l('Remove selected');
        $this->l('Delete permanently');
        $this->l('Restore selected');
        $this->l('Merge selected as new');
        $this->l('items');
        $this->l('Google Fonts');
        $this->l('Choose from hundreds of custom fonts faces provided by Google Fonts');
        $this->l('Load only on admin interface');
        $this->l('You haven\'t added any Google font to your library yet.');
        $this->l('Enter a font name to add to your collection');
        $this->l('Choose a font family');
        $this->l('Add font');
        $this->l('Back to results');
        $this->l('Cyrillic');
        $this->l('Cyrillic Extended');
        $this->l('Devanagari');
        $this->l('Greek');
        $this->l('Greek Extended');
        $this->l('Khmer');
        $this->l('Latin');
        $this->l('Latin Extended');
        $this->l('Vietnamese');
        $this->l('Select new');
        $this->l('Remove character set');
        $this->l('Use character sets:');
        $this->l('Troubleshooting &amp; Advanced Settings');
        $this->l('Don\'t change these options without experience, incorrect settings might break your site.');
        $this->l('Use slider markup caching');
        $this->l('Enabled caching can drastically increase the plugin performance and spare your server from unnecessary load.');
        $this->l('Empty caches');
        $this->l('Load uncompressed JS files');
        $this->l('Switch this option on if you want to debug the code.');
        $this->l('Use GreenSock (GSAP) sandboxing');
        $this->l('Enabling GreenSock sandboxing can solve issues when other plugins are using multiple/outdated versions of this library.');
        $this->l('Scripts priority');
        $this->l('Used to specify the order in which scripts are loaded. Lower numbers correspond with earlier execution.');
        $this->l('WebshopWorks Newsletter');
        $this->l('Stay Updated');
        $this->l('News about the latest features and other product info.');
        $this->l('Sneak Peak on Product Updates');
        $this->l('Access to all the cool new features before anyone else.');
        $this->l('Provide Feedback');
        $this->l('Participate in various programs and help us improving LayerSlider.');
        $this->l('Enter your email address');
        $this->l('Subscribe');
        $this->l('Product Support');
        $this->l('Read the documentation');
        $this->l('Get started with using LayerSlider.');
        $this->l('Browse the FAQs');
        $this->l('Find answers for common questions.');
        $this->l('Direct Support');
        $this->l('Get in touch with our Support Team.');
        $this->l('Unlock Now');
        $this->l('Contact the developer');
        $this->l('The documentation is here');
        $this->l('Open this help menu to quickly access to our online documentation.');
        $this->l('LayerSlider CSS Editor');
        $this->l('Contents of your custom CSS file');
        $this->l('You can type here custom CSS code, which will be loaded both on your admin and front-end pages.');
        $this->l('Please make sure to not override layout properties (positions and sizes), as they can interfere');
        $this->l('with the sliders built-in responsive functionality. Here are few example targets to help you get started:');
        $this->l('You need to make your uploads folder writable in order to save your changes.');
        $this->l('Using invalid CSS code could break the appearance of your site or your sliders. Changes cannot be reverted after saving.');
        $this->l('LayerSlider Transition Builder');
        $this->l('2D Transitions');
        $this->l('Type transition name');
        $this->l('No 2D transitions yet.');
        $this->l('3D Transitions');
        $this->l('No 3D transitions yet.');
        $this->l('Before you can save your changes, you need to make your "%s" folder writable.');
        $this->l('Transition Builder documentation');
        $this->l('To get started with the LayerSlider Transition Builder, please read our online documentation by clicking on this help menu.');
        $this->l('It looks like your files isn\'t writable, so PHP couldn\'t make any changes (CHMOD).');
        $this->l('Cannot write to file');
        $this->l('It looks like you haven\'t selected any skin to edit.');
        $this->l('No skin selected.');
        $this->l('All Sliders');
        $this->l('CSS Editor');
        $this->l('Transition Builder');
        $this->l('Server configuration issues detected!');
        $this->l('phpQuery, an external library in LayerSlider, have unmet dependencies. It requires PHP5 with the following extensions installed: PHP DOM extension, PHP Multibyte String extension. Please contact with your hosting provider to resolve these dependencies, as it will likely prevent LayerSlider from functioning properly.');
        $this->l('This issue could result a blank page in slider builder.');
        $this->l('The slider cannot be found');
        $this->l('Unpublished slider');
        $this->l('The slider you\'ve inserted here is yet to be published, thus it won\'t be displayed to your visitors. You can publish it by enabling the appropriate option in ');
        $this->l('Slider Settings -> Publish');
        $this->l('Removed slider');
        $this->l('The slider you\'ve inserted here was removed in the meantime, thus it won\'t be displayed to your visitors. This slider is still recoverable on the admin interface. You can enable listing removed sliders with the Screen Options -> Removed sliders option, then choose the Restore option for the corresponding item to reinstate this slider, or just click ');
        $this->l('Due to scheduling, this slider is no longer visible to your visitors. If you wish to reinstate this slider, just remove the schedule in ');
        $this->l('Premium features is available for preview purposes only.');
        $this->l('We\'ve detected that you\'re using premium features in this slider, but you have not yet activated your copy of LayerSlider. Premium features in your sliders will not be available for your visitors without activation. ');
        $this->l('Click here to learn more');
        $this->l('LayerSlider encountered a problem while it tried to show your slider.');
        $this->l('Please make sure that you\'ve used the right shortcode or method to insert the slider, and check if the corresponding slider exists and it wasn\'t deleted previously.');
        $this->l('Only you and other administrators can see this to take appropriate actions if necessary.');
        $this->l('Don\'t filter manufacturers');
        $this->l('Module Position');
        $this->l('Configure');
        $this->l('Slider will appear on the selected position.');
        $this->l('- None -');
        $this->l('Home');
        $this->l('Top of pages');
        $this->l('Banner');
        $this->l('Navigation');
        $this->l('Top column blocks');
        $this->l('Left column blocks');
        $this->l('Right column blocks');
        $this->l('Footer');
        $this->l('After body opening tag');
        $this->l('Navigation 1');
        $this->l('Navigation 2');
        $this->l('Navigation full-width');
        $this->l('Footer after');
        $this->l('Footer before');
        $this->l('Before body closing tag');
        $this->l('Database error, can\'t update hook!');
        $this->l('Language');
        $this->l('Slider will appear only on the selected language. (In case of multilanguage)');
        $this->l('- All -');
        $this->l('Shop');
        $this->l('Slider will appear only on the selected shop. (In case of Multi-shops)');
        $this->l('Show slider on these <br> Categories &amp; Pages');
        $this->l('Use Ctrl to select multiple categories or pages.');
        $this->l('Ordering');
        $this->l('Sliders in the same module position can be ordered with this option. Use lower value if you want to move this slider in above the others.');
        $this->l('Popularity');
        $this->l('Sold quantity');
        $this->l('Special offer');
        $this->l('Product name');
        $this->l('Product price');
        $this->l('Please buy the product to get the premium sliders!');
    }
}
