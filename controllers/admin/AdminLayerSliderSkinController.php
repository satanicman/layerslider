<?php
/**
* LayerSlider v6.1.9 - Responsive Slideshow Module http://layerslider.webshopworks.com
*
*  @author    WebshopWorks <info@webshopworks.com>
*  @copyright 2017 WebshopWorks
*  @license   One Domain Licence
*/

defined('_PS_VERSION_') or exit;

class AdminLayerSliderSkinController extends ModuleAdminController
{
    public function postProcess()
    {
        parent::postProcess();
        if (isset($this->context->cookie->ls_error)) {
            $this->errors[] = $this->context->cookie->ls_error;
            unset($this->context->cookie->ls_error);
        }
    }

    public function initModal()
    {
        $this->context->smarty->assign(array(
            'trad_link' => 'index.php?tab=AdminTranslations&token='.Tools::getAdminTokenLite('AdminTranslations').'&type=modules&lang=',
            'module_languages' => Language::getLanguages(false),
            'module_name' => 'layerslider',
        ));
        $this->modals[] = array(
            'modal_id' => 'moduleTradLangSelect',
            'modal_class' => 'modal-sm',
            'modal_title' => Translate::getAdminTranslation('Translate this module', 'AdminModules'),
            'modal_content' => $this->context->smarty->fetch('controllers/modules/modal_translation.tpl')
        );
        parent::initModal();
    }

    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_btn['translate'] = array(
            'href' => 'javascript:;',
            'desc' => Translate::getAdminTranslation('Translate', 'AdminModules'),
            'icon' => 'process-icon-flag',
            'js' => "jQuery('#moduleTradLangSelect').modal('show');"
        );
        parent::initPageHeaderToolbar();
    }

    public function setMedia()
    {
        parent::setMedia();

        $GLOBALS['ls_token'] = $this->token;
        $GLOBALS['ls_screen'] = (object) array(
          'id' => 'layerslider_page_ls-skin-editor',
          'base' => 'layerslider_page_ls-skin-editor'
        );
        // simulate wp page
        ${'_GET'}['page'] = 'ls-skin-editor';

        require_once _PS_MODULE_DIR_.$this->module->name.'/helper.php';
        require_once _PS_MODULE_DIR_.'layerslider/views/default.php';
    }

    public function display()
    {
        $this->context->smarty->assign(array('content' => $this->content));
        $this->display_footer = false;

        parent::display();
    }
}
