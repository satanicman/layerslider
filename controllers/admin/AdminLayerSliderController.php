<?php
/**
* LayerSlider v6.1.9 - Responsive Slideshow Module http://layerslider.webshopworks.com
*
*  @author    WebshopWorks <info@webshopworks.com>
*  @copyright 2017 WebshopWorks
*  @license   One Domain Licence
*/

defined('_PS_VERSION_') or exit;

class AdminLayerSliderController extends ModuleAdminController
{
    public function postProcess()
    {
        parent::postProcess();
        if (isset($this->context->cookie->ls_error)) {
            $this->errors[] = $this->context->cookie->ls_error;
            unset($this->context->cookie->ls_error);
        }
    }

    public function initModal()
    {
        $this->context->smarty->assign(array(
            'trad_link' => 'index.php?tab=AdminTranslations&token='.Tools::getAdminTokenLite('AdminTranslations').'&type=modules&lang=',
            'module_languages' => Language::getLanguages(false),
            'module_name' => 'layerslider',
        ));
        $this->modals[] = array(
            'modal_id' => 'moduleTradLangSelect',
            'modal_class' => 'modal-sm',
            'modal_title' => Translate::getAdminTranslation('Translate this module', 'AdminModules'),
            'modal_content' => $this->context->smarty->fetch('controllers/modules/modal_translation.tpl')
        );
        parent::initModal();
    }

    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_btn['translate'] = array(
            'href' => 'javascript:;',
            'desc' => Translate::getAdminTranslation('Translate', 'AdminModules'),
            'icon' => 'process-icon-flag',
            'js' => "jQuery('#moduleTradLangSelect').modal('show');"
        );
        parent::initPageHeaderToolbar();
    }

    public function setMedia()
    {
        parent::setMedia();

        $GLOBALS['ls_token'] = $this->token;
        $GLOBALS['ls_screen'] = (object) array(
          'id' => 'toplevel_page_layerslider',
          'base' => 'toplevel_page_layerslider'
        );
        // simulate wp page
        ${'_GET'}['page'] = 'layerslider';

        if (version_compare(_PS_VERSION_, '1.6', '<')) {
            // LayerSlider requires at least jQuery v1.8
            foreach ($this->context->controller->js_files as &$js) {
                if (preg_match('/jquery-\d\.\d\.\d(\.min)?\.js$/i', $js)) {
                    $js = __PS_BASE_URI__.'modules/layerslider/views/js/jquery.js';
                    break;
                }
            }
        }

        require_once _PS_MODULE_DIR_.$this->module->name.'/helper.php';
        require_once _PS_MODULE_DIR_.'layerslider/views/default.php';
    }

    public function display()
    {
        $tmpl = '<script type="text/html" id="tmpl-template-store">
            <div id="ls-importing-modal-window">
                <header>
                    <h1>'.ls__('Template Store', 'LayerSlider').'</h1>
                    <b class="dashicons dashicons-no"></b>
                </header>
                <div class="km-ui-modal-scrollable">
                    <p>
                        '.ls__('Premium templates are only available after you connected your site with PrestaShop\'s marketplace.', 'LayerSlider').'
                        <a href="https://www.youtube.com/watch?v=SLFFWyY2NYM" target="_blank" style="font-size:13px">'.ls__('Check this video for more details.').'</a>
                    </p>
                    <button class="button button-primary button-hero" id="btn-connect-ps">'.ls__('Connect to PrestaShop Addons', 'LayerSlider').'</button>
                </div>
            </div>
        </script>';
        $this->context->smarty->assign(array('content' => $tmpl.$this->content));
        $this->display_footer = false;

        parent::display();
    }
}
